import React from 'react'
import EmptyState from '@atlaskit/empty-state'

import image from '../../static/img/error-window.svg'

function NotFound () {
  return (
    <EmptyState
      size='narrow'
      header='That link has no power here'
      imageUrl={image}
    />
  )
}

export default NotFound
