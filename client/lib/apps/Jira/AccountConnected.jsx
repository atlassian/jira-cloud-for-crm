export default function AccountConnected () {
  const urlParams = new window.URLSearchParams(window.location.search)
  const accountId = urlParams.get('accountId')

  if (window.opener && typeof window.opener.manualConnect === 'function') {
    window.opener.manualConnect(accountId)
  } else {
    try {
      window.localStorage.setItem('accountId', accountId)
    } catch (error) {
      window.alert('Something went wrong.\nReload this page and try again')
    }
  }

  window.close()

  return null
}
