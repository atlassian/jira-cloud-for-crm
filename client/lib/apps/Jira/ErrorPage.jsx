export default function ErrorPage () {
  const urlParams = new window.URLSearchParams(window.location.search)
  const code = urlParams.get('code')
  const message = urlParams.get('message')
  const id = urlParams.get('error_id')
  const provider = urlParams.get('provider')

  const params = { code, message, id, provider }

  if (typeof window.opener.manualConnect === 'function') {
    window.opener.manualConnect(0, params)
  } else {
    try {
      window.localStorage.setItem('error', JSON.stringify(params))
    } catch (error) {
      window.alert(message || `Something went wrong. Error ID: ${id}`)
    }
  }

  window.close()

  return null
}
