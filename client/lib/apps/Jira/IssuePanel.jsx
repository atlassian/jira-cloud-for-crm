import React, { Component } from 'react'
import { bool, object, array } from 'prop-types'
import { inject, observer } from 'mobx-react'
import styled from 'styled-components'
import moment from 'moment'

import { Dialog, getContext } from '../../services/JiraAP'
import gas from '../../services/gas'

import Button from '@atlaskit/button'
import EditorAddIcon from '@atlaskit/icon/glyph/editor/add'
import EditorDoneIcon from '@atlaskit/icon/glyph/editor/done'
import EditorCloseIcon from '@atlaskit/icon/glyph/editor/close'
import InfoIcon from '@atlaskit/icon/glyph/info'
import EditorFeedbackIcon from '@atlaskit/icon/glyph/editor/feedback'

import { Card, ChildrenContainer } from '../../components/cards'
import Preloader from '../../components/Preloader'
import ReauthorizePrompt from '../../components/ReauthorizePrompt'

import { customersStoreType } from '../../stores/types'

const LinkWrapper = styled.div`
  margin-left: -2px;
  user-select: none;
`

@inject('customersStore')
@observer
export default class IssuePanel extends Component {
  static propTypes = {
    customersStore: customersStoreType.isRequired,
    isPreview: bool,
    provider: object,
    previewData: array
  }

  componentDidMount () {
    const { isPreview, provider, previewData, customersStore } = this.props

    if (isPreview) {
      return customersStore.loadPreviewData({ provider, previewData })
    }

    customersStore.loadCustomersInfo()

    gas.send({
      page: 'web-panel',
      name: `visit`
    })
  }

  componentWillReceiveProps ({ provider, previewData, customersStore }) {
    if (this.props.previewData !== previewData) {
      customersStore.loadPreviewData({ provider, previewData })
    }
  }

  openLinkDialog = async () => {
    const { jira: { issue, project } } = await getContext()
    const { provider, linkCustomer } = this.props.customersStore
    Dialog.match({
      projectId: project.id,
      issueId: issue.id,
      provider,
      callback: linkCustomer
    })
  }

  renderCard = (match, sub = '', i) => {
    const { provider, loadChildObjectInfo, unlinkCustomer, isPreview } = this.props.customersStore
    const { id, label, matchId, info, fields, values, objects } = match

    const head = {
      title: info.title,
      icon: provider.icons[id.toLowerCase()],
      iconLabel: label,
      link: info.url,
      actions: !isPreview ? [{
        text: 'Remove contact',
        callback: () => unlinkCustomer(matchId)
      }] : [],
      analytics: {
        provider: provider.id,
        recordType: id
      }
    }

    const valuesData = fields.map((field, i) => ({
      label: field.label,
      value: values
        ? formatValue(field.type, values[i], info.url)
        : field.label
    }))

    return <Card
      type={sub}
      head={head}
      values={valuesData}
      childObjects={<ChildrenContainer
        recordId={info.recordId}
        recordType={id}
        objects={objects}
        loadChildObjectInfo={loadChildObjectInfo}
        formatValue={formatValue}
        isPreview={isPreview}
        provider={provider.id}
      />}
      isPreview={isPreview}
      key={info.recordId + i}
    />
  }

  renderNestedCard = ({ account, related }) => {
    const { provider, loadChildObjectInfo, isPreview } = this.props.customersStore
    const head = {
      title: account.info.title,
      icon: provider.icons[account.id.toLowerCase()],
      iconLabel: account.label,
      link: account.info.url,
      analytics: {
        provider: provider.id,
        recordType: account.id
      }
    }

    const values = account.fields.map((field, i) => ({
      label: field.label,
      value: account.values
        ? formatValue(field.type, account.values[i], account.info.url)
        : field.label
    }))

    return <Card
      appearance='nested'
      head={head}
      values={values}
      childObjects={<ChildrenContainer
        recordId={account.info.recordId}
        recordType={account.id}
        objects={account.objects}
        loadChildObjectInfo={loadChildObjectInfo}
        formatValue={formatValue}
        isPreview={isPreview}
        provider={provider.id}
      />}
      items={related.map((match, i) => this.renderCard(match, 'sub', i))}
      isPreview={isPreview}
      key={account.info.recordId}
    />
  }

  render () {
    const {
      isProcessing,
      isConnected,
      needsAuthorization,
      projectConfigUrl,
      isPreview,
      customersInfo
    } = this.props.customersStore

    if (isProcessing) return <Preloader />

    if (needsAuthorization) {
      return (
        <div className='no-connection'>
          <ReauthorizePrompt projectConfigUrl={projectConfigUrl} />
        </div>
      )
    }

    if (!isConnected) {
      return (
        <div className='no-connection'>
          The CRM integration is not configured.
          <p>
            <InfoIcon size='small' primaryColor='#42526E' label='info' />&nbsp;
            If you are the administrator of this project, you&nbsp;
            <a href={projectConfigUrl} target='_blank'>can configure the integration here</a>.
          </p>
        </div>
      )
    }

    const Customers = customersInfo.length
      ? customersInfo.map(block => 'account' in block
        ? this.renderNestedCard(block)
        : this.renderCard(block)
      )
      : null

    const hasCustomers = !!customersInfo.length

    return (
      <div className='issue-panel'>
        {Customers}
        {!isPreview && <div className={`footer${hasCustomers ? ' has-contacts' : ''}`}>
          <LinkWrapper onClick={this.openLinkDialog}>
            <Button
              appearance='link'
              spacing='none'
              iconBefore={<EditorAddIcon size='small' label='Link customer' />}
            >
              Link customer
            </Button>
          </LinkWrapper>
          {hasCustomers && <Button
            appearance='subtle-link'
            onClick={() => Dialog.feedback.show({
              type: 'feedback',
              page: 'web-panel'
            })}
            iconBefore={<EditorFeedbackIcon label='Feedback' />}
            className='feedback-button'
          >
            Give feedback
          </Button>}
        </div>}
      </div>
    )
  }
}

function formatValue (type, value, url) {
  // We show dash if we get null or empty string
  // 0 and false are acceptable values
  if (value == null || value.length === 0) {
    value = '—'
    return value
  }

  if (type === 'title') {
    return <a href={url} target='_blank'>{value}</a>
  }

  if (type === 'related_item') {
    return <a href={value.url} target='_blank'>{value.name}</a>
  }

  if (type === 'email') {
    return <a href={`mailto:${value}`}>{value}</a>
  }

  if (type === 'url' && value) {
    // The URL acts like a relative link
    // if it doesn't have protocol specified
    const link = /^(http|https):\/\//.test(value) ? value : `https://${value}`
    const { hostname } = new window.URL(link)
    return <a href={link} target='_blank'>{hostname}</a>
  }

  if (type === 'percent') {
    return `${value}%`
  }

  if (type === 'datetime') {
    return value ? moment(value).format('DD/MMM/YY hh:mm A') : '—'
  }

  if (type === 'date') {
    return value ? moment(value).format('DD/MMM/YY') : '—'
  }

  if (type === 'boolean') {
    return value
      ? <EditorDoneIcon className='boolean-icon' primaryColor='#36B37E' label='Yes' />
      : <EditorCloseIcon className='boolean-icon' primaryColor='#FF5630' label='No' />
  }

  return value
}
