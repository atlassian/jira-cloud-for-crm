import React, { Component } from 'react'
import Preloader from '../../components/Preloader'
import FieldTextArea from '@atlaskit/field-text-area'
import RadioGroup from '@atlaskit/field-radio-group'

import { Dialog } from '../../services/JiraAP'
import gas from '../../services/gas'

const dictionary = {
  feedback: {
    heading: 'Give feedback about CRM for Jira Cloud',
    question: 'Tell us about your experience using CRM for Jira Cloud so that we can continue to improve it'
  },
  anotherCRM: {
    heading: 'Using another CRM?',
    question: 'Tell us which CRM you\'d like to see supported in CRM for Jira Cloud'
  }
}

export default class Feedback extends Component {
  state = {
    canContact: 'yes',
    feedback: '',
    type: '',
    page: ''
  }

  async componentDidMount () {
    Dialog.submitButton.disable()

    window.AP.dialog.getCustomData(({ type, page }) => {
      this.setState({ type, page })
    })

    window.AP.dialog.disableCloseOnSubmit()

    window.AP.events.on('dialog.submit', async () => {
      await gas.send({
        name: 'feedback',
        properties: { ...this.state },
        hash: false
      })

      window.AP.dialog.close(true)
    })
  }

  handleTextAreaChange = e => {
    const { value } = e.target
    this.setState({ feedback: e.target.value })

    value.length
      ? Dialog.submitButton.enable()
      : Dialog.submitButton.disable()
  }

  handleRadioChange = e => this.setState({ canContact: e.target.value })

  render () {
    const { feedback, canContact, type } = this.state

    if (!type) return <Preloader />

    const { heading, question } = dictionary[type]

    return (
      <div className='feedback-dialog'>
        <h2>{heading}</h2>
        <FieldTextArea
          label={question}
          value={feedback}
          onChange={this.handleTextAreaChange}
          minimumRows='5'
          shouldFitContainer
          autoFocus
        />
        <RadioGroup
          label='May we contact you to follow up on your feedback?'
          value={canContact}
          onRadioChange={this.handleRadioChange}
          name='may-contact'
          items={[
            { value: 'yes', label: 'Yes', defaultSelected: true },
            { value: 'no', label: 'No' }
          ]}
        />
      </div>
    )
  }
}
