import React from 'react'
import PropTypes from 'prop-types'
import Lozenge from '@atlaskit/lozenge'
import Avatar, { AvatarItem } from '@atlaskit/avatar'
import {
  TableRow,
  TableRowColumn
} from 'react-lightning-design-system'

export default function IssueRow ({ issue }) {
  const {
    link,
    key,
    summary,
    type,
    status,
    priority,
    reporter,
    assignee
  } = issue

  const { hostname } = new window.URL(link)
  const issueUrl = `https://${hostname}/browse/${key}`
  const reporterUrl = `https://${hostname}/people/${reporter.accountId}`
  const assigneeUrl = `https://${hostname}/people/${assignee.accountId}`

  return (
    <TableRow>
      <TableRowColumn>
        <a href={issueUrl} target='_blank'>{key}</a>
      </TableRowColumn>
      <TableRowColumn>
        <a href={issueUrl} target='_blank'>{summary}</a>
      </TableRowColumn>
      <TableRowColumn>
        {
          <div>
            <img src={type.iconUrl} className='jira-icon' />
            {type.name}
          </div>
        }
      </TableRowColumn>
      <TableRowColumn>
        <Lozenge appearance={status.type}>{status.name}</Lozenge>
      </TableRowColumn>
      <TableRowColumn>
        {
          <div>
            <img src={priority.iconUrl} className='jira-icon' />
            {priority.name}
          </div>
        }
      </TableRowColumn>
      <TableRowColumn className='user'>
        {
          <AvatarItem
            href={reporterUrl}
            target='_blank'
            avatar={<Avatar src={reporter.photo} size='xsmall' />}
            primaryText={reporter.name}
          />
        }
      </TableRowColumn>
      <TableRowColumn className='user'>
        {
          assignee.name
            ? <AvatarItem
              href={assigneeUrl}
              target='_blank'
              avatar={<Avatar src={assignee.photo} size='xsmall' />}
              primaryText={assignee.name}
            />
            : null
        }
      </TableRowColumn>
    </TableRow>
  )
}

IssueRow.propTypes = {
  issue: PropTypes.object.isRequired
}
