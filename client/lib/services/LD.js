import LDClient from 'ldclient-js'
import util from './util'
import config from '../config'

export default async function () {
  const { cloudId, user } = await util.getUserAndLocation()

  return LDClient.initialize(config.LDKey, {
    key: cloudId,
    name: user
  })
}
