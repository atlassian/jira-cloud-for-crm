const productionUrl = ''
const LD_PROD_KEY = ''
const LD_DEV_KEY = ''
const SENTRY_URL = ''

const environment = (() => {
  const { hostname } = window.location
  if (productionUrl.includes(hostname)) return 'production'
  if (hostname.includes('localhost') || hostname.includes('ngrok')) return 'development'
  return 'staging'
})()

function getLaunchDarklyKey () {
  return environment === 'production'
    ? LD_PROD_KEY
    : LD_DEV_KEY
}

export default {
  productionUrl,
  environment,
  release: window.BUILD.VERSION,
  sentryUrl: SENTRY_URL,
  LDKey: getLaunchDarklyKey()
}
