import React from 'react'
import { observable } from 'mobx'
import { observer, Provider } from 'mobx-react'
import { object, array, func, bool } from 'prop-types'

import Button from '@atlaskit/button'
import Tooltip from '@atlaskit/tooltip'
import Spinner from '@atlaskit/spinner'
import InlineDialog from '@atlaskit/inline-dialog'
import SectionMessage from '@atlaskit/section-message'
import EditorFeedbackIcon from '@atlaskit/icon/glyph/editor/feedback'

import Sync from './Sync'
import Tabs from './Tabs'
import IssuePanel from '../apps/Jira/IssuePanel'
import { Dialog } from '../services/JiraAP'
import customersStore from '../stores/customersStore'

@observer
export default class MainPage extends React.Component {
  static propTypes = {
    account: object.isRequired,
    connectAccount: func.isRequired,
    currentOptions: array.isRequired,
    saveOptions: func.isRequired,
    cancelOptions: func.isRequired,
    deleteConnection: func.isRequired,
    processing: object.isRequired,
    isManualSyncPanelVisible: bool.isRequired
  }

  @observable isDialogOpen = false

  render () {
    const {
      account,
      currentOptions,
      saveOptions,
      cancelOptions,
      deleteConnection,
      processing,
      isManualSyncPanelVisible
    } = this.props

    const dialogContent = (
      <div className='confirmation-dialog'>
        <h5>Are you sure you want to disconnect?</h5>
        <p>This will disable the integration for this project.</p>
        <div className='buttons'>
          <Button
            onClick={deleteConnection}
            appearance='primary'
            iconAfter={processing.deleting ? <Spinner /> : null}
            isDisabled={processing.deleting}>
            Confirm
          </Button>
          <Button
            onClick={() => (this.isDialogOpen = !this.isDialogOpen)}
            appearance='link'>
              Cancel
          </Button>
        </div>
      </div>
    )

    return (
      <div className='project-config-main'>
        <div className='header'>
          <div className='connection'>
            <h4 className='crmName'>{account.provider.name}</h4>
            <div className='status'>
              <span>Connected as&nbsp;</span>
              {
                account.user.name ? (
                  <Tooltip
                    content={account.user.email}
                    position='bottom'
                  >
                    <span className='username'>{account.user.name}</span>
                  </Tooltip>
                ) : (
                  <span className='username'>{account.user.email || 'Unknown'}</span>
                )
              }
              <InlineDialog
                content={dialogContent}
                isOpen={this.isDialogOpen}
                onContentBlur={() => (this.isDialogOpen = false)}
              >
                <Button onClick={() => (this.isDialogOpen = !this.isDialogOpen)}
                  appearance='subtle-link'
                >Disconnect</Button>
              </InlineDialog>
            </div>
          </div>
          {isManualSyncPanelVisible && <Sync stats={account.connection.stats} id={account.connection.id} />}
        </div>

        {account.needsAuthorization ? (
          <SectionMessage
            appearance='warning'
            actions={[{
              text: 'Reauthorize',
              key: 'reauthorize',
              onClick: () => this.props.connectAccount(account.type, account.domain)
            }]}
          >
            The user's account (<Button appearance='link' spacing='none' href={account.domain} target='_blank'>{account.domain}</Button>) connected to this project is inactive in the CRM or authorization with the CRM has expired. Please reauthorize this account to restore the connection.
          </SectionMessage>
        ) : (
          <div className='main-content'>
            <div className='tabs'>
              <Tabs schema={account.connection.options.schema} />
              <div className='action-buttons'>
                <Button
                  onClick={saveOptions}
                  appearance='primary'
                  iconAfter={processing.saving ? <Spinner /> : null}
                  isDisabled={processing.saving}>
                  Save
                </Button>
                <Button
                  onClick={cancelOptions}
                  appearance='link'>
                    Cancel
                </Button>
              </div>
            </div>
            <div className='preview'>
              <h3>Preview</h3>
              <div className='preview-box'>
                <Provider customersStore={customersStore}>
                  <IssuePanel
                    previewData={currentOptions}
                    provider={account.provider}
                    isPreview
                  />
                </Provider>
              </div>
              <div className='feedback'>
                <Button
                  appearance='subtle-link'
                  onClick={() => Dialog.feedback.show({
                    type: 'feedback',
                    page: 'project-config'
                  })}
                  iconBefore={<EditorFeedbackIcon label='Feedback' />}
                >
                  Give feedback
                </Button>
              </div>
            </div>
          </div>
        )}
      </div>
    )
  }
}
