import React from 'react'
import styled from 'styled-components'
import Spinner from '@atlaskit/spinner'

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`

export default function Preloader () {
  return <Wrapper>
    <Spinner size='medium' />
  </Wrapper>
}
