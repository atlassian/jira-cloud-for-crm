const path = require('path')
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const SentryCliPlugin = require('@sentry/webpack-plugin')

const assets = [
  'babel-polyfill',
  'whatwg-fetch',
  'url-search-params-polyfill',
  'url-polyfill'
]

const getVersion = () => {
  try {
    const { version } = require('../build-output/release-version.json')
    return version
  } catch (error) {
    return 'DUMMY'
  }
}

const version = getVersion()
const env = process.env['NODE_ENV']

module.exports = {
  mode: env,
  entry: [...assets, './lib/index.jsx'],
  output: {
    path: path.join(__dirname, 'static', 'dist'),
    filename: '[name].[chunkhash].js'
  },
  resolve: {
    modules: ['.', 'node_modules'],
    extensions: ['.js', '.jsx']
  },
  devtool: 'source-map',
  module: {
    rules: [
      {
        test: /\.(js|jsx)?$/,
        use: [
          'babel-loader?cacheDirectory=true',
          'eslint-loader?cache=true'
        ],
        exclude: /node_modules/
      },
      {
        test: /\.scss$/,
        use: ['style-loader', 'css-loader', 'sass-loader']
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      },
      {
        test: /\.(svg|jpeg|png|gif?)(\?[a-z0-9=&.]+)?$/,
        use: ['file-loader?publicPath=../&name=./img/[hash].[ext]']
      },
      {
        test: /\.(ttf|otf|eot|woff(2)?)(\?[a-z0-9=&.]+)?$/,
        use: ['file-loader?publicPath=../&name=./fonts/[hash].[ext]']
      },
      {
        test: /\.(png|jpg|gif)$/,
        use: ['url-loader?limit=8192']
      }
    ]
  },
  plugins: [
    new CleanWebpackPlugin(),
    new webpack.DefinePlugin({
      'window.BUILD': JSON.stringify({
        VERSION: version
      })
    }),
    new SentryCliPlugin({
      release: version,
      include: 'static/dist',
      dryRun: true,
      silent: true
    }),
    new HtmlWebpackPlugin({
      template: './lib/template.html'
    }),
    new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/)
  ]
}
