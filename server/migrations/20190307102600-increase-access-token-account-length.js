module.exports = {
  async up (queryInterface, DataTypes) {
    await queryInterface.sequelize.transaction(async (transaction) => {
      await queryInterface.changeColumn('Accounts', 'accessToken', {
        type: DataTypes.STRING(3000),
        allowNull: false,
      }, { transaction })

      await queryInterface.changeColumn('Accounts', 'refreshToken', {
        type: DataTypes.STRING(3000),
        allowNull: false,
      }, { transaction })
    })
  },

  async down (queryInterface, DataTypes) {
    await queryInterface.sequelize.transaction(async (transaction) => {
      await queryInterface.changeColumn('Accounts', 'accessToken', {
        type: DataTypes.STRING(2000),
        allowNull: false,
      }, { transaction })

      await queryInterface.changeColumn('Accounts', 'refreshToken', {
        type: DataTypes.STRING(2000),
        allowNull: false,
      }, { transaction })
    })
  },
}
