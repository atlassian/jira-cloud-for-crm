module.exports = {
  async up (queryInterface, DataTypes) {
    return queryInterface.sequelize.transaction(async (t) => {
      const options = { transaction: t }

      await queryInterface.addColumn('Connections', 'totalIssues', {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0,
      }, options)

      await queryInterface.addColumn('Connections', 'checkedIssues', {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0,
      }, options)

      await queryInterface.addColumn('Connections', 'matchedIssues', {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0,
      }, options)
    })
  },

  async down (queryInterface) {
    await queryInterface.removeColumn('Connections', 'totalIssues')
    await queryInterface.removeColumn('Connections', 'checkedIssues')
    await queryInterface.removeColumn('Connections', 'matchedIssues')
  },
}
