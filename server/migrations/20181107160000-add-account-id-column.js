module.exports = {
  up (queryInterface, DataTypes) {
    return queryInterface.addColumn('Accounts', 'jiraAccountId', {
      type: DataTypes.STRING,
      allowNull: true,
    })
  },

  down (queryInterface) {
    return queryInterface.removeColumn('Accounts', 'jiraAccountId')
  },
}
