module.exports = {
  async up (queryInterface) {
    await queryInterface.removeColumn('Accounts', 'userKey')
    await queryInterface.addIndex('Accounts', ['jiraAccountId'])
  },

  async down (queryInterface, DataTypes) {
    return queryInterface.addColumn('Accounts', 'userKey', {
      type: DataTypes.STRING,
      allowNull: true,
    })
  },
}
