module.exports = (sequelize, DataTypes) => {
  const Connection = sequelize.define('Connection', {
    instanceId: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    accountId: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    projectId: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    options: {
      type: DataTypes.JSONB,
      allowNull: false,
    },
  }, {
    indexes: [
      { fields: ['instanceId'] },
      { fields: ['accountId'] },
      { fields: ['projectId'] },
      { fields: ['createdAt'] },
    ],
  })

  Connection.associate = function associate (models) {
    this.belongsTo(models.Instance, { foreignKey: 'instanceId' })
    this.belongsTo(models.Account, { foreignKey: 'accountId' })
    this.Matches = this.hasMany(models.Match, {
      foreignKey: 'connectionId',
      onDelete: 'cascade',
    })
  }

  return Connection
}
