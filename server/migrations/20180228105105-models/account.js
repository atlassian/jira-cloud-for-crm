module.exports = (sequelize, DataTypes) => {
  const Account = sequelize.define('Account', {
    instanceId: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    userKey: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    type: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    baseUrl: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    externalId: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    crmUserId: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    crmUserData: {
      type: DataTypes.JSONB,
    },
    accessToken: {
      type: DataTypes.STRING(2000),
      allowNull: false,
    },
    refreshToken: {
      type: DataTypes.STRING(2000),
      allowNull: false,
    },
    tokenCreatedAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW,
    },
  }, {
    indexes: [
      { fields: ['instanceId'] },
      { fields: ['userKey'] },
      { fields: ['type'] },
      { fields: ['baseUrl'] },
      { fields: ['externalId'] },
      { fields: ['crmUserId'] },
      { fields: ['createdAt'] },
    ],
  })

  Account.associate = function associate (models) {
    this.belongsTo(models.Instance, { foreignKey: 'instanceId' })
    this.Connections = this.hasMany(models.Connection, {
      foreignKey: 'accountId',
      onDelete: 'cascade',
    })
  }

  return Account
}
