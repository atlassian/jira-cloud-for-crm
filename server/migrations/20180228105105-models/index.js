const Sequelize = require('sequelize')
const Memcached = require('memcached')
const Bluebird = require('bluebird')
const sequelizeCache = require('sequelize-transparent-cache')
const MemcachedAdaptor = require('sequelize-transparent-cache-memcached')
const { lowerFirst } = require('lodash')

const { postgres, memcached } = require('../../lib/common/config').get()

module.exports = ({ logger }) => {
  const cache = new Memcached(
    `${memcached.host}:${memcached.port}`,
    { namespace: memcached.namespace }
  )

  const memcachedAdaptor = new MemcachedAdaptor({
    client: cache,
    namespace: 'model',
    lifetime: memcached.ttl,
  })

  const { withCache } = sequelizeCache(memcachedAdaptor)

  const opts = {
    dialect: 'postgres',
    // To enable logs use logger.info.bind(logger) as a value
    logging: false,
    replication: {
      write: postgres.write,
      read: [postgres.read],
    },
    define: {
      paranoid: true,
      indexes: [
        { fields: ['deletedAt'] },
      ],
    },
  }

  const sequelize = new Sequelize(postgres.database, postgres.user, postgres.password, opts)

  const models = ['Instance', 'Account', 'Connection', 'Match']

  const db = models.reduce((acc, modelName) => {
    acc[modelName] = withCache(sequelize.import(`./${lowerFirst(modelName)}`))

    return acc
  }, {})

  Object.keys(db).forEach((modelName) => {
    const model = db[modelName]

    if (model.associate) {
      model.associate(db)
    }
  })

  sequelize.cache = Bluebird.promisifyAll(cache)
  db.sequelize = sequelize
  db.Sequelize = Sequelize

  return db
}
