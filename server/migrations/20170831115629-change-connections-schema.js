const salesforceSchema = [
  {
    id: 'Contact',
    label: 'Contact',
    fields: [
      { id: 'Email', label: 'Email', type: 'email' },
    ],
    objects: [
      {
        id: 'Case',
        label: 'Cases',
        fields: [
          { id: 'CaseNumber', label: 'Case Number', type: 'title' },
          { id: 'Status', label: 'Status', type: 'picklist' },
        ],
      },
      {
        id: 'Opportunity',
        label: 'Opportunities',
        fields: [
          { id: 'Name', label: 'Name', type: 'title' },
          { id: 'StageName', label: 'Stage', type: 'picklist' },
          { id: 'CloseDate', label: 'Close Date', type: 'date' },
        ],
      },
    ],
  },
  {
    id: 'Lead',
    label: 'Lead',
    fields: [
      { id: 'Email', label: 'Email', type: 'email' },
    ],
    objects: [],
  },
  {
    id: 'Account',
    label: 'Account',
    fields: [
      { id: 'Type', label: 'Account Type', type: 'picklist' },
    ],
    objects: [
      {
        id: 'Case',
        label: 'Cases',
        fields: [
          { id: 'CaseNumber', label: 'Case Number', type: 'title' },
          { id: 'Status', label: 'Status', type: 'picklist' },
        ],
      },
      {
        id: 'Contact',
        label: 'Contacts',
        fields: [
          { id: 'Name', label: 'Full Name', type: 'title' },
          { id: 'Email', label: 'Email', type: 'email' },
        ],
      },
      {
        id: 'Opportunity',
        label: 'Opportunities',
        fields: [
          { id: 'Name', label: 'Name', type: 'title' },
          { id: 'StageName', label: 'Stage', type: 'picklist' },
          { id: 'CloseDate', label: 'Close Date', type: 'date' },
        ],
      },
    ],
  },
]

module.exports = {
  async up (queryInterface, DataTypes) {
    await queryInterface.sequelize.query(
      `update "Connections" set "options"='{"schema": ${JSON.stringify(salesforceSchema)}}' where "options" = '{}' or "options" is null`
    )

    await queryInterface.changeColumn(
      'Connections',
      'options',
      {
        type: DataTypes.JSON,
        allowNull: false,
      }
    )
  },

  async down (queryInterface, DataTypes) {
    await queryInterface.changeColumn(
      'Connections',
      'options',
      {
        type: DataTypes.JSON,
        allowNull: true,
      }
    )
  },
}
