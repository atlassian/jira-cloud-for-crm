const contactFields = [
  {
    id: 'name',
    label: 'Name',
    type: 'title',
  },
  {
    id: 'email',
    label: 'Email',
    type: 'email',
  },
]

module.exports = {
  async up (queryInterface) {
    const connections = await queryInterface.sequelize.query(
      `SELECT "Connections"."id", "options" FROM "Connections" INNER JOIN "Accounts" ON "Connections"."accountId" = "Accounts"."id" WHERE "Accounts"."type" = 'hubspot'`,
      { type: queryInterface.sequelize.QueryTypes.SELECT }
    )

    const promises = connections.map(async (connection) => {
      const groups = connection.options.schema.map((group) => {
        group.objects = group.objects.map((object) => {
          if (object.id === 'Contact') {
            object.fields = [...contactFields]
          }

          return object
        })

        return group
      })

      await queryInterface.sequelize.query(
        `UPDATE "Connections" SET "options"='{"schema": ${JSON.stringify(groups)}}' WHERE "id"=${connection.id}`
      )
    })

    await Promise.all(promises)
  },
}
