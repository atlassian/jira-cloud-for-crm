const url = require('url')
const passport = require('passport')
const SalesforceStrategy = require('passport-salesforce').Strategy
const Route = require('../Route')
const { addon, crm } = require('../../common/config').get('')

const options = {
  clientID: crm.salesforce.clientId,
  clientSecret: crm.salesforce.clientSecret,
  callbackURL: new URL('/api/accounts/create/salesforce/callback', addon.localBaseUrl).href,
}

const strategy = new SalesforceStrategy(
  options,
  (accessToken, refreshToken, profile, done) => done(null, ({
    tokens: {
      accessToken,
      refreshToken,
    },
    profile,
  }))
)

passport.use(strategy)

module.exports = class extends Route {
  authenticate (req, res, next) {
    const authenticate = passport.authenticate('salesforce', {
      session: false,
      state: req.query['addon-token'],
    })

    authenticate(req, res, (error) => {
      if (error) return this._authErrorMiddleware(error, req, res)
      next()
    })
  }

  async callback (req, res) {
    try {
      const { hostname } = new url.URL(req.user.profile.profile)

      const account = await this.initService('accounts.Create', {
        type: 'salesforce',
        tokens: req.user.tokens,
        externalId: req.user.profile.organization_id,
        crmUserId: req.user.profile.user_id,
        baseUrl: `https://${hostname}`,
      })
        .run(req)

      const pageUrl = url.format({
        host: addon.localBaseUrl,
        pathname: '/account_connected',
        query: { accountId: account.id },
      })

      res.redirect(pageUrl)
    } catch (error) {
      this._authErrorMiddleware(error, req, res)
    }
  }

  _authErrorMiddleware (error, req, res) {
    const { code, message } = error

    if (code && message) {
      if (
        ['API_CURRENTLY_DISABLED', 'API_DISABLED_FOR_ORG'].includes(code) ||
        (code === 'invalid_grant' && message === 'authentication failure')
      ) {
        req.logger.warn(message)
      } else {
        req.logger.error({ err: error }, message)
      }

      res.redirect(`/error?code=${code}&message=${message}&error_id=${req.id}&provider=salesforce`)
    } else {
      req.logger.error({ err: error }, 'Salesforce authentication error.')
      res.redirect(`/error?error_id=${req.id}`)
    }
  }
}
