const Route = require('./Route')

module.exports = class extends Route {
  delete (req, res) {
    this.initService('instances.Delete').run(req, res)
  }

  create (req, res) {
    const instance = Object.assign(
      { id: req.body.clientKey },
      req.body
    )

    this.initService('instances.Create', instance)
      .run(req, res)
  }
}
