const jwtAuthentication = require('jwt-authentication')
const Route = require('../Route')

const { service, jwt } = require('../../common/config').get()

const jwtValidator = jwtAuthentication.server.create({
  publicKeyBaseUrl: jwt.publicKeyBaseUrl || jwt.publicKeyFallbackBaseUrl,
  resourceServerAudience: jwt.audience,
})

const asapAuthorizedSubjects = [
  'micros/opshub',
]

module.exports = class Auth extends Route {
  check (req, res, next) {
    const { logger } = req

    if (service.environment === 'development') {
      return next()
    }

    const jwtAuthenticate = jwtAuthentication.middleware.create(
      jwtValidator,
      asapAuthorizedSubjects,
      logger
    )

    jwtAuthenticate(req, res, () => {
      const authenticated = req.claims.roles.includes(`${jwt.audience}:admin`)

      if (!authenticated) {
        return res.sendStatus(401)
      }

      logger.info('opshub-trail', {
        sub: req.claims.sub,
        method: req.method,
        url: req.originalUrl,
        params: req.params,
        query: req.query,
      })

      next()
    })
  }
}
