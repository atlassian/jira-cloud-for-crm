const jwt = require('atlassian-jwt')
const moment = require('moment')
const Route = require('../Route')

module.exports = class extends Route {
  constructor (data, options) {
    super(data)
    this.options = Object.assign(this.defaultOptions, options)
  }

  get defaultOptions () {
    return {
      skipNoToken: false,
      skipNoInstance: false,
    }
  }

  async check (req, res, next) {
    if (!req.context) {
      req.context = {}
    }

    try {
      const extractedContext = await this._extractContextFromToken(req)

      Object.assign(req.context, extractedContext)
      req.logger = req.logger.child({ context: req.context })

      next()
    } catch (error) {
      req.logger.warn({ err: error }, 'Session check error')

      res.status(401).json({
        error: {
          message: error.message,
          code: 'AUTHENTICATION_FAILED',
        },
      })
    }
  }

  validateToken (instance, token) {
    const data = jwt.decode(token, instance.sharedSecret)
    const now = moment().utc().unix()

    if (data.exp && now > data.exp) {
      throw new Error('Session expired')
    }

    return data
  }

  async _extractContextFromToken (req) {
    const token = this.extractToken(req)

    if (!token) {
      if (this.options.skipNoToken) {
        return
      }

      throw new Error('Token not found')
    }

    const instanceId = jwt.decode(token, '', true).iss
    const instance = await this._getInstance(req.context, instanceId)

    if (!instance) {
      if (this.options.skipNoInstance) {
        return
      }

      throw new Error('Unknown instance')
    }

    const data = this.validateToken(instance, token, req)

    return this.extractContext(data)
  }

  async _getInstance (context, instanceId) {
    if (!context.instance) {
      context.instance = await this.model.Instance.cache().findByPk(instanceId)
    }

    return context.instance
  }
}
