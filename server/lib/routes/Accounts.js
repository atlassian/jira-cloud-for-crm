const Route = require('./Route')

module.exports = class extends Route {
  show (req, res) {
    this.initService('accounts.Show', {
      projectId: req.query.projectId,
    })
      .run(req, res)
  }
}
