const url = require('url')
const moment = require('moment')

const decode = require('salesforce-signed-request')
const jwt = require('atlassian-jwt')

const Route = require('../Route')

const {
  crm: { salesforce: { clientSecret } },
} = require('../../common/config').get()

module.exports = class extends Route {
  notApprovedOauth (req, res) {
    res.redirect('/salesforce_issues_list?oauthRequired=true')
  }

  async init (req, res) {
    try {
      const salesforceSession = decode(req.body.signed_request, clientSecret)

      const { instanceUrl } = salesforceSession.client
      const { userId } = salesforceSession.context.user
      const externalId = salesforceSession.context.organization.organizationId
      const recordType = salesforceSession.context.environment.record.attributes.type
      const recordId = salesforceSession.context.environment.record.Id

      const connection = await this.model.Connection.findOne({
        include: {
          model: this.model.Account,
          where: {
            externalId,
          },
        },
      })

      if (!connection) {
        return res.redirect('/salesforce_issues_list?oauthRequired=true')
      }

      const now = moment().utc()

      const addonSession = {
        iss: externalId,
        aud: externalId,
        iat: now.unix(),
        exp: now.add(1, 'hours').unix(),
        sub: {
          instanceUrl,
          userId,
          externalId,
        },
      }

      const pageUrl = url.format({
        pathname: '/salesforce_issues_list',
        query: {
          jwt: jwt.encode(addonSession, clientSecret),
          recordType,
          recordId,
        },
      })

      res.redirect(pageUrl)
    } catch (error) {
      return res.send({
        error: { code: 'UNAUTHORIZED' },
      })
    }
  }

  check (req, res, next) {
    try {
      const addonSession = jwt.decode(req.query.jwt, clientSecret)

      req.context = addonSession.sub
      req.logger = req.logger.child({ context: req.context })
      next()
    } catch (error) {
      return res.send({
        error: { code: 'UNAUTHORIZED' },
      })
    }
  }
}
