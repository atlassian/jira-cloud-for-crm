const Route = require('../Route')
const addonDescriptor = require('../../common/addonDescriptor')

module.exports = class extends Route {
  get (req, res) {
    res.json(addonDescriptor)
  }
}
