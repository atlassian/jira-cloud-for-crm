const uuid = require('uuid/v1')
const { get, pick } = require('lodash')
const onFinished = require('on-finished')
const Route = require('./Route')
const { environment } = require('../common/config').get('service')
const logger = require('../common/logger').child({ module: 'addon' })

module.exports = class extends Route {
  errorHandler (error, req, res, next) {
    // In case if error happens before Middleware.logger is initialized
    if (!req.logger) {
      req.id = uuid()
      req.logger = logger.child({
        req,
        req_id: req.id,
      })
    }

    const expectedErrors = [
      'request entity too large',
    ]

    if (expectedErrors.includes(error.message)) {
      req.logger.warn({ err: error }, 'Bad request')

      return res.status(error.status || 400).json({ error: {
        message: error.message || 'Bad request',
        code: 'BAD_REQUEST',
      } })
    }

    req.logger.error({ err: error }, 'Unexpected error (express level)')

    res.status(500).json({ error: {
      message: 'Something went wrong! Please try again later.',
      code: 'UNEXPECTED_ERROR',
    } })
  }

  logger (req, res, next) {
    req.id = uuid()

    req.logger = logger.child({
      req,
      res,
      req_id: req.id,
    })

    // We want to skip logging requests for static files or healthchecks
    const allowedToLog = req.url.startsWith('/api') &&
      !req.url.startsWith('/api/healthcheck')

    if (!['development', 'test'].includes(environment) && allowedToLog) {
      const startTime = Date.now()

      onFinished(res, () => {
        const endTime = Date.now()
        const totalTime = endTime - startTime

        res.responseTime = totalTime

        req.logger.child({
          serializers,
        }).info({ req, res, context: req.context }, 'request.info')
      })
    }

    next()
  }
}

const serializers = {
  req (req) {
    return {
      method: req.method,
      url: req.url,
      ...{ webhookEvent: get(req, 'body.webhookEvent') },
    }
  },

  context (context) {
    return {
      instance: pick(context.instance, ['id', 'baseUrl']),
      jiraAccountId: get(context, 'jiraAccountId'),
    }
  },
}
