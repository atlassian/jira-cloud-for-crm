const Route = require('./Route')

module.exports = class extends Route {
  create (req, res) {
    this.initService('matches.Create', {
      projectId: req.body.projectId,
      issueId: req.body.issueId,
      recordId: req.body.recordId,
      recordType: req.body.recordType,
    })
      .run(req, res)
  }

  delete (req, res) {
    this.initService('matches.Delete', {
      projectId: req.body.projectId,
      matchId: req.body.matchId,
    })
      .run(req, res)
  }
}
