const Route = require('./Route')

module.exports = class extends Route {
  show (req, res) {
    this.initService('customers.Show', {
      issueId: req.params.issueId,
      projectId: req.params.projectId,
      recordId: req.query.recordId,
      recordType: req.query.recordType,
      childRecordType: req.query.childRecordType,
    })
      .run(req, res)
  }

  list (req, res) {
    this.initService('customers.List', {
      projectId: req.query.projectId,
      issueId: req.query.issueId,
      search: req.query.search,
    })
      .run(req, res)
  }
}
