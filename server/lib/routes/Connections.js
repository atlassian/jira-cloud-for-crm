const Route = require('./Route')

module.exports = class extends Route {
  create (req, res) {
    this.initService('connections.Create', {
      accountId: req.body.accountId,
      projectId: req.body.projectId,
    })
      .run(req, res)
  }

  update (req, res) {
    this.initService('connections.Update', {
      id: req.params.id,
      options: req.body.options,
    })
      .run(req, res)
  }

  delete (req, res) {
    this.initService('connections.Delete', {
      id: req.params.id,
    })
      .run(req, res)
  }

  sync (req, res) {
    this.initService('connections.Sync', {
      id: req.params.id,
    })
      .run(req, res)
  }
}
