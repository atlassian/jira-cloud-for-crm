const { pickBy, get } = require('lodash')
const querystring = require('querystring')
const jwt = require('jsonwebtoken')
const moment = require('moment')
const client = require('./client')('dynamics')
const config = require('../config')

const dynamicsConfig = config.get('crm.dynamics')
const { environment } = config.get('service')

const GUIDFields = [
  'contactid',
  'accountid',
  'leadid',
  'opportunityid',
  'incidentid',
  '_parentaccountid_value',
  '_accountid_value',
  '_parentcustomerid_value',
  '_customerid_value',
  '_parentcontactid_value',
]

class Dynamics {
  constructor (account) {
    this.account = account
  }

  whoAmI () {
    return this._fetch('whoAmI', { pathname: '/api/data/v8.2/WhoAmI()' })
  }

  getMyself () {
    return this._fetch('getMyself', {
      pathname: `/api/data/v8.2/systemusers(${this.account.crmUserId})`,
      query: {
        $select: 'fullname,internalemailaddress',
      },
    })
  }

  getObjectFieldsMetadata (recordType) {
    const query = {
      $select: 'SchemaName',
      $expand: 'Attributes($select=AttributeType,LogicalName,DisplayName)',
      $filter: `SchemaName eq '${recordType}'`,
    }

    return this._fetch('getObjectFieldsMetadata', { pathname: '/api/data/v8.2/EntityDefinitions', query })
  }

  getContacts (where, fields = ['emailaddress1']) {
    const query = this._buildOdataQuery(where, fields)

    return this._fetchCollection('getContacts', 'Contact', { pathname: '/api/data/v8.2/contacts', query })
  }

  getAccounts (where, fields = []) {
    const query = this._buildOdataQuery(where, fields)

    return this._fetchCollection('getAccounts', 'Account', { pathname: '/api/data/v8.2/accounts', query })
  }

  getIncidents (where, fields = []) {
    const query = this._buildOdataQuery(where, fields)

    return this._fetchCollection('getIncidents', 'Incident', { pathname: '/api/data/v8.2/incidents', query })
  }

  getLeads (where, fields = []) {
    const query = this._buildOdataQuery(where, fields)

    return this._fetchCollection('getLeads', 'Lead', { pathname: '/api/data/v8.2/leads', query })
  }

  getOpportunities (where, fields = []) {
    const query = this._buildOdataQuery(where, fields)

    return this._fetchCollection('getOpportunities', 'Opportunity', { pathname: '/api/data/v8.2/opportunities', query })
  }

  getNewToken () {
    const body = querystring.stringify({
      grant_type: 'refresh_token',
      client_id: dynamicsConfig.clientId,
      client_secret: dynamicsConfig.clientSecret,
      refresh_token: this.account.refreshToken,
      resource: this.account.baseUrl,
    })

    const headers = {
      Authorization: null,
      'Content-Type': 'application/x-www-form-urlencoded',
    }

    return this._fetch(
      'getNewToken',
      { host: 'https://login.microsoftonline.com', pathname: '/common/oauth2/token' },
      { method: 'POST', body, headers }
    )
  }

  _buildOdataQuery (filterParams, selectFields) {
    // GUID field types should not be wrapped in quotes
    const wrap = (key, value) => (GUIDFields.includes(key) ? value : `'${value}'`)

    const $filter = Object.keys(filterParams)
      .map((key) => {
        const value = filterParams[key]

        if (typeof value === 'object') {
          // handle odata function
          return `${key}(${value.args.join(',')})`
        }

        return `${key} eq ${wrap(key, value)}`
      })
      .join(' and ')

    const $select = selectFields.join(',')

    return pickBy({ $filter, $select })
  }

  _isTokenExpired (state) {
    const wwwAuthenticate = get(state, 'res.headers.www-authenticate[0]', '')
    const accessTokenExpired = wwwAuthenticate.includes('Security Token Expired')

    if (accessTokenExpired) return true

    // Don't proceed in tests
    if (environment === 'test') return false

    const { exp } = jwt.decode(this.account.accessToken)
    const now = moment().utc().unix()

    return exp - now < 0
  }

  async _fetchCollection (methodName, entityType, urlParams) {
    // Dynamics Odata API does not return requested entity type in response
    // so let's put this entityType to every returned item by ourselves
    const response = await this._fetch(methodName, urlParams)

    return response.value.map(item => Object.assign(item, {
      id: item[`${entityType.toLowerCase()}id`], // dynamics entities ids have names leadid, contactid etc
      entityType,
    }))
  }

  async _fetch (
    methodName,
    { host, pathname, query },
    { method = 'GET', body, headers = {} } = {}
  ) {
    const url = new URL(pathname, host || this.account.baseUrl)

    if (query) {
      url.search = querystring.stringify(query)
    }

    if (headers['Content-Type'] === undefined) {
      headers['Content-Type'] = 'application/json'
    }

    if (headers.Authorization === undefined) {
      headers.Authorization = `Bearer ${this.account.accessToken}`
    }

    if (body && headers['Content-Type'] === 'application/json') {
      body = JSON.stringify(body)
    }

    const state = {
      req: {
        method,
        headers,
        body,
        url: url.toString(),
      },
      res: {},
    }

    try {
      await client(state, methodName)

      return state.res.body
    } catch (error) {
      const fields = {
        originError: error,
        source: 'dynamics',
      }

      const accessTokenExpired = this._isTokenExpired(state)

      // Don't request another token if the first attempt is failed
      if (methodName !== 'getNewToken' && accessTokenExpired) {
        const { access_token: accessToken, refresh_token: refreshToken } = await this.getNewToken()

        await this.account.update({ accessToken, refreshToken })

        return this._fetch.apply(this, arguments) // eslint-disable-line
      }

      const errorDescription = get(state, 'res.body.error_description', '')
      const refreshTokenExpired = errorDescription.includes('refresh token has expired due to inactivity')

      if (refreshTokenExpired) {
        fields.code = 'CRM_ACCOUNT_INACTIVE'
      }

      const invalidClient = get(state, 'res.body.error') === 'invalid_client' && errorDescription.includes('Error validating credentials')

      if (invalidClient) {
        fields.code = 'INVALID_CLIENT'
      }

      throw Object.assign(
        new Error('Dynamics API error'),
        state,
        fields
      )
    }
  }
}

module.exports = Dynamics
