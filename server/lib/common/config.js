const convict = require('convict')
const pkg = require('./../../package.json')

const config = convict({
  service: {
    name: {
      default: pkg.name,
      doc: 'Service name',
    },
    microsServiceName: {
      default: pkg.name,
      doc: 'Micros service name',
      env: 'MICROS_SERVICE',
      format: String,
    },
    version: {
      default: pkg.version,
      doc: 'Service version',
    },
    environment: {
      default: 'development',
      doc: 'Environment the service is running in',
      env: 'NODE_ENV',
      format: ['development', 'test', 'production'],
    },
    microsVersion: {
      default: 'local',
      doc: 'Version of the service',
      env: 'MICROS_SERVICE_VERSION',
      format: String,
    },
    microsEnvironment: {
      default: 'local',
      doc: 'Micros environment',
      env: 'MICROS_ENV',
    },
    microsEnvironmentType: {
      default: 'local',
      doc: 'Micros Environment Type',
      env: 'MICROS_ENVTYPE',
    },
    microsDeploymentId: {
      default: 'localDev',
      doc: 'Id of the Micros deployment',
      env: 'MICROS_DEPLOYMENT_ID',
    },
    microsAWSRegion: {
      default: 'local',
      doc: 'Micros aws region',
      env: 'MICROS_AWS_REGION',
    },
    microsGroup: {
      default: 'WebServer',
      doc: 'Micros group',
      env: 'MICROS_GROUP',
      format: String,
    },
    albKeepAliveIdleTimeout: {
      default: 60000 * 2, // https://hello.atlassian.net - Node 8+ on ALB - fix your timeouts
      doc: 'How long to keep socket connection alive in milliseconds',
      env: 'KEEP_ALIVE_TIMEOUT',
      format: 'nat',
    },
    isExternalApiCallsCached: {
      default: true,
      doc: 'Disables cache for tests',
      format: Boolean,
    },
  },

  crm: {
    salesforce: {
      clientId: {
        default: 'DUMMY',
        doc: 'Salesforce app client id',
        format: String,
        env: 'CRM_SALESFORCE_CLIENT_ID',
      },
      clientSecret: {
        default: 'DUMMY',
        doc: 'Salesforce app client secret',
        format: String,
        env: 'CRM_SALESFORCE_CLIENT_SECRET',
      },
      allowedDomains: [
        '.force.com',
        '.salesforce.com',
        '.visualforce.com',
      ],
    },
    dynamics: {
      clientId: {
        default: 'DUMMY',
        doc: 'MS Dynamics app client id',
        format: String,
        env: 'CRM_DYNAMICS_CLIENT_ID',
      },
      clientSecret: {
        default: 'DUMMY',
        doc: 'MS Dynamics app client secret',
        format: String,
        env: 'CRM_DYNAMICS_CLIENT_SECRET',
      },
    },
    hubspot: {
      clientId: {
        default: 'DUMMY',
        doc: 'HUBSPOT app client id',
        format: String,
        env: 'CRM_HUBSPOT_CLIENT_ID',
      },
      clientSecret: {
        default: 'DUMMY',
        doc: 'HUBSPOT app client secret',
        format: String,
        env: 'CRM_HUBSPOT_CLIENT_SECRET',
      },
    },
  },

  addon: {
    localBaseUrl: {
      default: 'http://localhost:8080',
      doc: 'Web app base url',
      format: 'url',
      env: 'ADDON_BASE_URL',
    },
    key: {
      default: process.env.ADDON_KEY
        ? `${pkg.name}-${process.env.ADDON_KEY}`
        : pkg.name,
      doc: 'AC add-on key',
      format: String,
    },
    name: {
      default: process.env.ADDON_KEY
        ? `Jira CRM Integration (${process.env.ADDON_KEY})`
        : 'Jira CRM Integration',
      doc: 'Jira CRM Integration',
      format: String,
    },
    env: {
      default: process.env.ADDON_KEY,
      doc: 'Raw add-on key w/o package name. Could be your local add-on key or ddev. Undefined on prod-east',
      format: String,
    },
    allowedDomains: [
      '.jira-dev.com',
      '.atlassian.net',
      '.atlassian.com',
      '.jira.com',
    ],
  },

  statsd: {
    host: {
      default: 'platform-statsd',
      doc: 'Micros Statsd Host',
      format: String,
    },
    port: {
      default: '8125',
      doc: 'Micros Statsd port',
      format: 'port',
    },
  },

  memcached: {
    host: {
      default: 'localhost',
      doc: 'Memcached port',
      env: 'MEMCACHED_CACHE_CONFIGHOST',
      format: String,
    },
    port: {
      default: 11211,
      doc: 'Memcached port',
      env: 'MEMCACHED_CACHE_CONFIGPORT',
      format: 'port',
    },
    ttl: {
      default: 60 * 60 * 2,
      doc: 'Memcached time to live',
      env: 'MEMCACHED_CACHE_TTL',
      format: 'duration',
    },
    namespace: {
      default: '',
      doc: 'Memcached keys prefix',
      env: 'MEMCACHED_CACHE_NAMESPACE',
      format: String,
    },
  },

  postgres: {
    write: {
      database: {
        default: pkg.name,
        doc: 'DB name',
        env: 'PG_DB_SCHEMA',
        format: String,
      },
      username: {
        default: 'postgres',
        doc: 'DB user name',
        env: 'PG_DB_ROLE',
        format: String,
      },
      password: {
        default: 'postgres',
        doc: 'DB user password',
        env: 'PG_DB_PASSWORD',
        format: String,
      },
      host: {
        default: 'localhost',
        doc: 'DB host',
        env: 'PG_DB_HOST',
        format: String,
      },
      port: {
        default: 5432,
        doc: 'DB port',
        env: 'PG_DB_PORT',
        format: 'port',
      },
    },
    read: {
      database: {
        default: pkg.name,
        doc: 'DB name',
        env: 'PG_DB_SCHEMA',
        format: String,
      },
      username: {
        default: 'postgres',
        doc: 'DB user name',
        env: 'PG_DB_READROLE',
        format: String,
      },
      password: {
        default: 'postgres',
        doc: 'DB user password',
        env: 'PG_DB_READPASSWORD',
        format: String,
      },
      host: {
        default: 'localhost',
        doc: 'DB host',
        env: 'PG_DB_HOST',
        format: String,
      },
      port: {
        default: 5432,
        doc: 'DB port',
        env: 'PG_DB_PORT',
        format: 'port',
      },
    },
  },

  sqs: {
    currentQueueType: {
      default: '',
      format: String,
      env: 'MICROS_GROUP',
      doc: 'Micros worker queue based on MICROS_GROUP',
    },
    queues: {
      GetIssues: {
        url: {
          default: 'http://localhost:9324/queue/GetIssues',
          doc: 'SQS queue URL',
          env: 'WORKER_GetIssues_QUEUE_URL',
          format: 'url',
        },
        name: {
          default: 'GetIssues',
          doc: 'SQS queue name',
          env: 'WORKER_GetIssues_QUEUE_NAME',
          format: String,
        },
      },
      SyncIssue: {
        url: {
          default: 'http://localhost:9324/queue/SyncIssue',
          doc: 'SQS queue URL',
          env: 'WORKER_SyncIssue_QUEUE_URL',
          format: 'url',
        },
        name: {
          default: 'SyncIssue',
          doc: 'SQS queue name',
          env: 'WORKER_SyncIssue_QUEUE_NAME',
          format: String,
        },
      },
      // StreamHub consumer
      StHubIn: {
        url: {
          default: 'http://localhost:9324/queue/StHubIn',
          doc: 'SQS queue URL',
          env: 'SQS_STREAMHUB_CONSUMER_QUEUE_URL',
          format: 'url',
        },
        name: {
          default: 'StHubIn',
          doc: 'SQS queue name',
          env: 'SQS_STREAMHUB_CONSUMER_QUEUE_NAME',
          format: String,
        },
      },
    },
    endpoint: {
      default: 'http://localhost:9324',
      doc: 'AWS base url, NOT used in production',
      env: 'DEV_SQS_BASE_URL',
      format: String,
    },
    region: {
      default: 'us-east-1',
      doc: 'AWS region',
      env: 'MICROS_AWS_REGION',
      format: String,
    },
  },

  jwt: {
    keyId: {
      default: '',
      doc: 'ASAP public key id',
      env: 'ASAP_KEY_ID',
      format: String,
    },
    privateKey: {
      default: '',
      doc: 'ASAP private key',
      env: 'ASAP_PRIVATE_KEY',
      format: String,
    },
    publicKeyBaseUrl: {
      default: 'https://s3-us-west-1.amazonaws.com/asap-public-key-repo.us-west-1.uswest.staging.atl-inf.io/',
      doc: 'ASAP public key repository base url',
      env: 'ASAP_PUBLIC_KEY_REPOSITORY_URL',
      format: String,
    },
    publicKeyFallbackBaseUrl: {
      default: '',
      doc: 'ASAP public key repository base url',
      env: 'ASAP_PUBLIC_KEY_FALLBACK_REPOSITORY_URL',
      format: String,
    },
    audience: {
      default: pkg.name,
      doc: 'ASAP service audience',
      env: 'ASAP_AUDIENCE',
      format: String,
    },
    issuer: {
      default: pkg.name,
      doc: 'ASAP service issuer',
      env: 'ASAP_ISSUER',
      format: String,
    },
  },
})

config.validate({ allowed: 'strict' })

module.exports = config
