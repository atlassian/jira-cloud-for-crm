const pino = require('pino')
const { copyWithoutSecrets } = require('../services/util')
const loggerName = require('./config').get('service.name')

const isProduction = process.env.NODE_ENV === 'production'

const serializers = {
  req ({ body, params, query, method, url, headers }) {
    return copyWithoutSecrets({ body, params, query, method, url, headers })
  },

  res ({ statusCode, responseTime }) {
    return copyWithoutSecrets({ statusCode, responseTime })
  },

  context: copyWithoutSecrets,
  sqsMessage: copyWithoutSecrets,

  err (err) {
    const serializedError = {
      ...err, // Custom enumerable fields
      ...pino.stdSerializers.err(err), // Default error fields
    }

    if (err.originError) {
      serializedError.originError = serializers.err(err.originError)
    }

    return copyWithoutSecrets(serializedError)
  },
}

const devOptions = {
  base: false, // Show only logger name
  prettyPrint: {
    colorize: true,
    translateTime: 'HH:MM:ss.l',
  },
}

const rootLogger = pino({
  name: loggerName,
  ...!isProduction && devOptions,
  serializers,
})

/* istanbul ignore if */
if (process.env.LOG_LEVEL) {
  rootLogger.level = process.env.LOG_LEVEL
}

module.exports = rootLogger
