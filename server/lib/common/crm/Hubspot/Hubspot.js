const { URL } = require('url')
const { get } = require('lodash')
const CRM = require('../CRM')
const defaultSchema = require('./defaultSchema.json')

class Hubspot extends CRM {
  apiCheck () {
    return true
  }

  async getMyself () {
    const { user } = await this.crmApi.getTokenInfo()

    return { email: user }
  }

  async getParentFields (parentObjectId) {
    const fields = await this.crmApi.getObjectFieldsMetadata(parentObjectId)

    return fields
      .filter(field => !field.hidden)
      .map((field) => {
        if (field.name === 'website') {
          field.type = 'url'
        }

        if (field.type === 'bool') {
          field.type = 'boolean'
        }

        return {
          id: field.name,
          label: field.label,
          type: field.type,
        }
      })
  }

  async findCustomers (search) {
    const response = await this.crmApi.getContactsList(search)

    return response.contacts.map((contact) => {
      // get photo from string like `hubfs/3881032/263765ce-5954-499e-a54f-0f4af23717de.png`
      const photoId = get(contact, 'properties.hs_avatar_filemanager_key.value', '').split('/')[2]

      let photoUrl

      if (photoId) {
        photoUrl = `https://cdn2.hubspot.net/hub/${this.account.externalId}/hubfs/${photoId}/height=144/width=144`
      } else {
        photoUrl = get(contact, 'properties.photo.value') || get(contact, 'properties.twitterprofilephoto.value') || ''
      }

      const firstname = get(contact, 'properties.firstname.value')
      const lastname = get(contact, 'properties.lastname.value')

      return {
        recordType: 'Contact',
        recordId: contact.vid.toString(),
        name: this._getCustomerName(firstname, lastname),
        email: get(contact, 'properties.email.value'),
        account: get(contact, 'properties.company.value'),
        photo: photoUrl,
      }
    })
  }

  async findMatchContactId (email) {
    const contact = await this.crmApi.getContactByEmail(email)

    return contact ? contact.vid.toString() : null
  }

  async getCustomerEmail (recordType, recordId) {
    const contact = await this.crmApi.getContactById(recordId, ['email'])

    return get(contact, 'properties.email.value')
  }

  async getParentValues (schema, recordId, recordType) {
    const { fields } = this._mapSchemaFields(schema).find(group => group.id === recordType)

    if (recordType === 'Contact') {
      const data = await this.crmApi.getContactById(recordId, fields)

      if (!data) return

      return this._lookupEnumerationFields(data, recordType)
    }
  }

  async getChildValues (schema, parentRecordId, parentRecordType, childRecordType) {
    const { fields } = this._mapSchemaFields(schema)[0]

    const methods = {
      Contact: {
        Deal: 'getDealsByContact',
      },
      Company: {
        Contact: 'getContactsByCompany',
        Deal: 'getDealsByCompany',
      },
    }

    const methodName = methods[parentRecordType][childRecordType]
    const response = await this.crmApi[methodName](parentRecordId, fields)

    if (childRecordType === 'Contact') {
      return this._formatContactsByCompany(Object.values(response)[0])
    }

    return this._lookupEnumerationFields(Object.values(response)[0], childRecordType)
  }

  mapCustomerData (schema, data, isChild) {
    const mapGroupFields = (groupSchema, groupData) => {
      if (!groupData) return []

      return groupSchema.fields.map((field) => {
        let property

        // sometimes hubspot return array of fields
        // and sometimes map of fields
        if (Array.isArray(groupData.properties)) {
          property = groupData.properties.find(prop => prop.name === field.id) || {}
        } else {
          property = groupData.properties[field.id] || {}
        }

        if (field.type === 'datetime' || field.type === 'date') {
          return Number(property.value)
        }

        return property.value
      })
    }

    const getObjectUrl = (group, objectId) => {
      const url = new URL(
        `/contacts/${this.account.externalId}/${group.id.toLowerCase()}/${objectId}`,
        'https://app.hubspot.com'
      )

      return url.toString()
    }

    if (!isChild) {
      return schema.map((group) => {
        const groupData = group.id === 'Contact'
          ? data
          : data['associated-company']

        if (group.id === 'Company' && !groupData) return

        const values = mapGroupFields(group, groupData)
        const info = {}

        if (group.id === 'Contact') {
          const firstname = get(groupData, 'properties.firstname.value')
          const lastname = get(groupData, 'properties.lastname.value')

          info.recordId = groupData.vid.toString()
          info.title = this._getCustomerName(firstname, lastname)
        }

        if (group.id === 'Company') {
          info.recordId = groupData['company-id'].toString()
          info.title = get(groupData, 'properties.name.value', 'Company')
        }

        info.url = getObjectUrl(group, info.recordId)

        return { ...group, info, values }
      })
    }

    return schema.map((group) => {
      const values = data.map(record => mapGroupFields(group, record))
      const url = data.map(record => getObjectUrl(group, record.dealId || record.vid))

      return { ...group, values, url }
    })
  }

  formatJiraIssue (issue) {
    const { hostname } = new URL(issue.self)
    const issueUrl = `https://${hostname}/browse/${issue.key}`

    const statusMap = {
      new: 'DEFAULT',
      indeterminate: 'INFO',
      done: 'SUCCESS',
    }

    const properties = Object.entries({
      Summary: issue.fields.summary,
      Assignee: get(issue, 'fields.assignee.displayName'),
      Reporter: get(issue, 'fields.reporter.displayName'),
      Priority: get(issue, 'fields.priority.name'),
      Type: get(issue, 'fields.issuetype.name'),
    }).map(([key, value]) => ({
      dataType: 'STRING',
      label: key,
      value: value || 'Unassigned',
    }))

    return {
      objectId: issue.id,
      title: issue.key,
      link: issueUrl,
      properties: properties.slice(0, 1).concat(
        {
          dataType: 'STATUS',
          label: 'Status',
          value: get(issue, 'fields.status.name'),
          optionType: statusMap[get(issue, 'fields.status.statusCategory.key')] || 'DEFAULT',
        },
        properties.slice(1)
      ),
    }
  }

  async _lookupEnumerationFields (data, objectId) {
    // hubspot returns field id instead of value
    // this method is used to lookup that value

    if (objectId === 'Deal') {
      // for Deals we only need to lookup Stage field
      const promises = data.map(async (deal) => {
        const pipelineId = deal.properties.pipeline.value
        const stageId = deal.properties.dealstage.value
        const pipeline = await this.crmApi.getPipelineById(pipelineId)

        deal.properties.dealstage.value = pipeline.stages
          .find(stage => stage.stageId === stageId).label

        return deal
      })

      return Promise.all(promises)
    }

    const metadata = await this.crmApi.getObjectFieldsMetadata(objectId)

    Object.keys(data.properties).forEach((propertyName) => {
      const fieldMetadata = metadata.find(field => field.name === propertyName)

      if (get(fieldMetadata, 'type') === 'enumeration') {
        const option = fieldMetadata.options.find(
          ({ value }) => value === data.properties[propertyName].value
        )

        if (!option) return
        data.properties[propertyName].value = option.label
      }
    })

    return data
  }

  _mapSchemaFields (schema) {
    return schema.map(group => ({
      id: group.id,
      fields: group.fields.map(field => field.id),
    }))
  }

  _formatContactsByCompany (data) {
    // HubSpot API doesn't expose full customer data when we're looking for them by company
    // The only fields we can get are first- and lastname, and email.
    // So we concatinate firstname and lastname to get fullname,
    // retrieve email from identities and rewrite properties to match them our scheme

    return data.map((contact) => {
      const { value: email } = contact.identities[0].identity.find(identity => identity.type === 'EMAIL')
      const firstname = contact.properties.find(props => props.name === 'firstname')
      const lastname = contact.properties.find(props => props.name === 'lastname')

      contact.properties = [
        {
          name: 'name',
          value: this._getCustomerName(get(firstname, 'value'), get(lastname, 'value')),
        },
        {
          name: 'email',
          value: email,
        },
      ]

      return contact
    })
  }

  _getCustomerName (firstname, lastname) {
    const nameData = [firstname, lastname]
    const fullname = nameData.filter(part => part).join(' ')

    return fullname.length ? fullname : 'Customer'
  }
}

Hubspot.defaultSchema = defaultSchema

module.exports = Hubspot
