const logger = require('./common/logger').child({ module: 'web' })
const initRoutes = require('./routes')
const initModel = require('./model')
const { albKeepAliveIdleTimeout } = require('./common/config').get('service')

function listenHttpPort (app, port) {
  return new Promise((resolve, reject) => {
    const server = app.listen(port, (error) => {
      if (error) {
        return reject(error)
      }

      resolve(server)
    })

    server.keepAliveTimeout = albKeepAliveIdleTimeout
    server.timeout = albKeepAliveIdleTimeout
  })
}

async function start (port) {
  const model = initModel({ logger })
  const app = initRoutes({ model, logger })

  const [server] = await Promise.all([
    listenHttpPort(app, port),
    model.sequelize.authenticate(),
  ])

  return { server, model, app }
}

if (!module.parent) {
  process.on('uncaughtException', (error) => {
    logger.fatal({ err: error }, 'Uncaught exception')
    process.exit(1)
  })

  process.on('unhandledRejection', (error) => {
    logger.error({ err: error }, 'Unhandled rejection')
  })

  const port = 8080

  start(port).then(() => logger.info(`Add-on server is running on ${port}`))
}

module.exports = { start }
