/* eslint-disable no-unreachable */
const Jira = require('../../common/net/Jira')

async function getProjectAdminPermission ({ instance, jiraAccountId }, projectId) {
  // TODO: figure out why it doesn't work sometimes
  return true

  const jira = new Jira(instance)

  async function fetchAndSaveToken () {
    const { token, expiresIn } = await jira.getUserToken(jiraAccountId)

    await instance.userToken(jiraAccountId, token, expiresIn - 30)

    return token
  }

  async function fetchAndSavePermission () {
    const permission = await jira.asUser(token).getProjectAdminPermission(projectId)

    await instance.userPermission(jiraAccountId, permission)

    return permission
  }

  const token =
    (await instance.userToken(jiraAccountId)) ||
    (await fetchAndSaveToken())

  const permission =
    (await instance.userPermission(jiraAccountId)) ||
    (await fetchAndSavePermission())

  return permission
}

module.exports = { getProjectAdminPermission }
