const Service = require('../Service')
const ServiceError = require('../Error')
const { getProjectAdminPermission } = require('./util')
const getCRMClass = require('../../common/crm')
const { sendMessage } = require('../../common/sqs/client')

module.exports = class extends Service {
  validate (params) {
    const rules = {
      accountId: ['required', 'positive_integer'],
      projectId: ['required', 'string'],
    }

    return this.validator(params, rules)
  }

  async execute ({ accountId, projectId }) {
    const account = await this.model.Account.findOne({
      where: {
        id: accountId,
        instanceId: this.context('instance.id'),
      },
      include: {
        model: this.model.Connection,
        where: { projectId },
        required: false,
      },
    })

    if (!account) {
      throw new ServiceError('Wrong account id', {
        code: 'WRONG_ID',
        fields: ['accountId'],
      })
    }

    const hasProjectAdminPermission = await getProjectAdminPermission(
      this.context(),
      projectId
    )

    if (!hasProjectAdminPermission) {
      throw new ServiceError('You have no access to project related to this connection', {
        code: 'PERMISSION_DENIED',
      })
    }

    if (account.Connections.length) {
      const jira = this.api('Jira', this.context('instance'))

      await jira.setProjectProperty(projectId, true)

      const { id, options } = account.Connections[0]

      this.logger.info({ accountId, projectId }, 'Connection already exists')

      return {
        id,
        accountId,
        projectId,
        options,
      }
    }

    const createdConnection = await this.model.sequelize.transaction(async (t) => {
      const CRM = getCRMClass(account.type)
      const crm = new CRM(this.api.bind(this), account)

      const connection = await this.model.Connection.create({
        accountId,
        projectId,
        options: {
          schema: await crm.getDefaultSchema(),
        },
        instanceId: this.context('instance.id'),
      }, { transaction: t })

      const jira = this.api('Jira', this.context('instance'))

      await jira.setProjectProperty(projectId, true)

      this.logger.info({ accountId, projectId }, 'Connection was created')

      const { id, options } = connection

      return {
        id,
        accountId,
        projectId,
        options,
      }
    })

    await sendMessage('GetIssues', {
      data: {
        connectionId: createdConnection.id,
        projectId: createdConnection.projectId,
      },
      context: this.context(),
    })

    return createdConnection
  }
}
