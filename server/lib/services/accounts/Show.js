const Service = require('../Service')
const getCRMClass = require('../../common/crm')

const formatConnection = ({
  id,
  projectId,
  options,
  totalIssues,
  checkedIssues,
  matchedIssues,
}) => ({
  id,
  projectId,
  options,
  stats: {
    total: totalIssues,
    checked: checkedIssues,
    matched: matchedIssues,
  },
})

module.exports = class extends Service {
  validate (params) {
    const rules = {
      projectId: ['required', 'string'],
    }

    return this.validator(params, rules)
  }

  async execute ({ projectId }) {
    const account = await this.model.Account.findOne({
      include: {
        model: this.model.Connection,
        where: { projectId },
      },
      where: {
        instanceId: this.context('instance.id'),
      },
    })

    if (!account) return

    const [connection] = account.Connections

    const result = {
      id: account.id,
      type: account.type,
      connection: formatConnection(connection),
      user: {},
    }

    if (!account.isAuthorized) {
      return {
        ...result,
        domain: account.baseUrl,
        needsAuthorization: true,
      }
    }

    const CRM = getCRMClass(account.type)
    const crm = new CRM(this.api.bind(this), account)

    try {
      const [fields, user] = await Promise.all([
        crm.getFields(connection.options.schema),
        crm.getMyself(),
      ])

      return {
        ...result,
        user,
        fields,
      }
    } catch (error) {
      if (error.code === 'CRM_ACCOUNT_INACTIVE') {
        account.deactivate()
        this.logger.info({ err: error, accountId: account.id }, 'Account is deactivated')
      }

      if (['CRM_ACCOUNT_INACTIVE', 'INVALID_CLIENT'].includes(error.code)) {
        return {
          ...result,
          domain: account.baseUrl,
          needsAuthorization: true,
        }
      }

      throw error
    }
  }
}
