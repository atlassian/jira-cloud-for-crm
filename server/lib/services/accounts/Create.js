const Service = require('../Service')
const ServiceError = require('../Error')
const getCRMClass = require('../../common/crm')

module.exports = class extends Service {
  validate (params) {
    const rules = {
      type: ['required', 'string'],
      tokens: ['required', 'any_object'],
      externalId: ['required', 'string'],
      crmUserId: ['required', 'string'],
      baseUrl: ['required', 'url'],
    }

    return this.validator(params, rules)
  }

  async execute ({ type, tokens, baseUrl, externalId, crmUserId }) {
    const instanceId = this.context('instance.id')
    const { jiraAccountId } = this.context()
    const { accessToken, refreshToken } = tokens

    const account = await this.model.Account.findOne({
      where: {
        instanceId,
        jiraAccountId,
        externalId,
      },
    })

    if (account) {
      await account.update({
        accessToken,
        refreshToken,
        tokenCreatedAt: new Date(),
        baseUrl,
        crmUserId,
        jiraAccountId,
      })

      this.logger.info({ baseUrl, id: account.id }, 'Account was updated')

      return {
        id: account.id,
      }
    }

    return this.model.sequelize.transaction(async (t) => {
      const createdAccount = await this.model.Account.create({
        type,
        instanceId,
        jiraAccountId,
        baseUrl,
        externalId,
        crmUserId,
        refreshToken,
        accessToken,
      }, { transaction: t })

      const CRM = getCRMClass(createdAccount.type)
      const crm = new CRM(this.api.bind(this), createdAccount)

      try {
        await crm.healthcheck()
      } catch (error) {
        const [{ errorCode, message }] = error.res.body

        throw new ServiceError(message, { code: errorCode })
      }

      this.logger.info({ baseUrl, id: createdAccount.id }, 'Account was created')

      return {
        id: createdAccount.id,
      }
    })
  }
}
