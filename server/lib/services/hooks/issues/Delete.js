const Service = require('../../Service')
const validationRules = require('./validationRules')

module.exports = class extends Service {
  validate (params) {
    const rules = {
      issue: validationRules.issue,
    }

    return this.validator(params, rules)
  }

  async execute ({ issue }) {
    const connection = await this.model.Connection.findOne({
      include: {
        model: this.model.Account,
        required: true,
      },
      where: {
        instanceId: this.context('instance.id'),
        projectId: issue.fields.project.id,
      },
    })

    if (!connection) return

    await this.model.Match.destroy({
      where: {
        connectionId: connection.id,
        issueId: issue.id,
      },
      force: true,
    })
  }
}
