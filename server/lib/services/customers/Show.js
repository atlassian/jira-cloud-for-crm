const Service = require('../Service')
const ServiceError = require('../Error')
const getCRMClass = require('../../common/crm')
const CRMError = require('../../common/crm/Error')

module.exports = class extends Service {
  validate (params) {
    const rules = {
      issueId: ['required', 'string'],
      projectId: ['required', 'string'],
      recordId: ['string'],
      recordType: ['string'],
      childRecordType: ['string'],
    }

    return this.validator(params, rules)
  }

  async execute ({ issueId, projectId, recordId, recordType, childRecordType }) {
    const connection = await this.model.Connection.findOne({
      include: [{
        model: this.model.Account,
        where: {
          instanceId: this.context('instance.id'),
        },
      }, {
        model: this.model.Match,
        where: { issueId },
        required: false,
      }],
      where: { projectId },
    })

    if (!connection) {
      throw new ServiceError('No connection for current project', {
        code: 'NO_CONNECTION',
      })
    }

    const account = connection.Account

    if (!account.isAuthorized) {
      throw new ServiceError('CRM account needs authorization', {
        code: 'INVALID_CLIENT',
      })
    }

    try {
      const CRM = getCRMClass(account.type)
      const crm = new CRM(this.api.bind(this), account)

      const { schema } = connection.options

      if (childRecordType) {
        return await crm.getValues(schema, recordId, recordType, childRecordType)
      }

      const customers = await this._getCustomers({ connection, crm })

      return {
        accountType: account.type,
        customers: customers.filter(Boolean),
      }
    } catch (error) {
      if (error.code === 'CRM_ACCOUNT_INACTIVE') {
        account.deactivate()
        this.logger.info({ err: error, accountId: account.id }, 'Account is deactivated')
      }

      if (['CRM_ACCOUNT_INACTIVE', 'INVALID_CLIENT'].includes(error.code)) {
        throw new ServiceError('CRM account needs authorization', {
          code: 'INVALID_CLIENT',
        })
      }

      if (error instanceof CRMError) {
        throw new ServiceError(error.message, { code: error.code })
      }

      throw error
    }
  }

  async _getCustomers ({ connection, crm }, retries = 2) {
    if (retries === 0) {
      this.logger.info('Too much retries while getting customers')

      return []
    }

    const { schema } = connection.options

    try {
      return await Promise.all(connection.Matches.map(async (match) => {
        const values = await crm.getValues(schema, match.recordId, match.recordType)

        if (!values) return

        return {
          values,
          matchId: match.id,
          isManual: match.isManual,
        }
      }))
    } catch (error) {
      if (!(error instanceof CRMError || error.code === 'INVALID_FIELD')) {
        throw error
      }

      // Salesforce specific code
      // Field id and object type of deleted field
      const { fieldId, objectId } = error.data

      const updatedSchema = schema.map((object) => {
        if (object.id === objectId) {
          return {
            ...object,
            fields: object.fields.filter(field => field.id !== fieldId),
          }
        }

        return object
      })

      connection.set('options.schema', updatedSchema)
      await connection.save()

      this.logger.info(
        { connectionId: connection.id, fieldId, objectId },
        'Removed redundant field'
      )

      return this._getCustomers({ connection, crm }, --retries)
    }
  }
}
