const { get } = require('lodash')

const Service = require('../Service')
const getCRMClass = require('../../common/crm')

module.exports = class extends Service {
  validate (params) {
    const rules = {
      issue: ['required', 'any_object'],
      connectionId: ['required', 'positive_integer'],
    }

    return this.validator(params, rules)
  }

  async execute ({ issue, connectionId }) {
    await this.model.Connection.increment('checkedIssues', {
      where: { id: connectionId },
    })

    const reporter = get(issue, 'fields.reporter')

    const jira = this.api('Jira', this.context('instance'))
    const email = reporter && (
      reporter.emailAddress || await jira.getUserEmail(reporter.accountId)
    )

    if (!email) return

    const matchContactId = await this._findMatchedContact({
      connectionId,
      email,
    })

    if (!matchContactId) return

    const created = await this.model.Match.upsert({
      recordType: 'Contact',
      recordId: matchContactId,
      connectionId,
      issueId: issue.id,
    })

    if (created) {
      await this.model.Connection.increment('matchedIssues', {
        where: { id: connectionId },
      })
    }
  }

  async _findMatchedContact ({ connectionId, email }) {
    const cacheTTL = 60 * 60 // 1 hour
    const cacheKey = `sync:${connectionId}:${email}`

    const cachedValue = await this.model.sequelize.cache.getAsync(cacheKey)

    // We cache 'null' if there's no match for this email
    // So, 'null' is a valid value
    if (cachedValue || cachedValue === null) {
      return cachedValue
    }

    const account = await this.model.Account.findOne({
      include: {
        model: this.model.Connection,
        where: { id: connectionId },
      },
    })

    if (!account) return

    const CRM = getCRMClass(account.type)
    const crm = new CRM(this.api.bind(this), account)

    try {
      const contactId = await crm.findMatchContactId(email)

      await this.model.sequelize.cache.setAsync(cacheKey, contactId || null, cacheTTL)

      return contactId
    } catch (error) {
      this.logger.warn(
        { err: error },
        'Error while getting contact in SyncIssue'
      )
    }
  }
}
