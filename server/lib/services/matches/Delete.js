const Service = require('../Service')
const ServiceError = require('../Error')

module.exports = class extends Service {
  validate (params) {
    const rules = {
      projectId: ['required', 'string'],
      matchId: ['required', 'positive_integer'],
    }

    return this.validator(params, rules)
  }

  async execute ({ projectId, matchId }) {
    const connection = await this.model.Connection.findOne({
      where: {
        instanceId: this.context('instance.id'),
        projectId,
      },
    })

    if (!connection) {
      throw new ServiceError('Wrong project id', {
        code: 'WRONG_ID',
        fields: ['projectId'],
      })
    }

    await this.model.Match.destroy({
      where: { id: matchId },
      force: true,
    })
  }
}
