const Service = require('../../Service')
const ServiceError = require('../../Error')

module.exports = class extends Service {
  validate (params) {
    const rules = {
      id: ['required', 'string'],
      force: [{ one_of: [true, false] }],
    }

    return this.validator(params, rules)
  }

  async execute ({ id, force = false }) {
    const instance = await this.model.Instance.findOne({
      where: { id },
      paranoid: false,
    })

    if (!instance) {
      throw new ServiceError('Invalid instance id', {
        code: 'WRONG_ID',
        fields: ['id'],
      })
    }

    await instance.cache().destroy({ force })
  }
}
