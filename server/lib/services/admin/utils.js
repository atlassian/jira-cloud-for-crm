
const { pick } = require('lodash/fp')

const formatAccount = pick([
  'id',
  'jiraAccountId',
  'type',
  'baseUrl',
  'externalId',
  'crmUserId',
  'tokenCreatedAt',
  'createdAt',
  'deletedAt',
  'updatedAt',
])

const formatInstance = pick([
  'id',
  'description',
  'baseUrl',
  'status',
  'serverVersion',
  'pluginsVersion',
  'createdAt',
  'deletedAt',
  'updatedAt',
])

const formatConnection = pick([
  'id',
  'projectId',
  'options',
  'createdAt',
  'deletedAt',
  'updatedAt',
])

function sortableBy (fields) {
  return fields.reduce((acc, field) => acc.concat([
    `-${field}`,
    `+${field}`,
    field,
  ]), [])
}

function sortBy (order) {
  return order.map((param) => {
    const field = param.replace(/^[+-]/, '')
    const direction = param.startsWith('-') ? 'DESC' : 'ASC'

    return [
      field,
      direction,
    ]
  })
}

module.exports = {
  formatAccount,
  formatInstance,
  formatConnection,
  sortableBy,
  sortBy,
}
