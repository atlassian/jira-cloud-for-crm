const Memcached = require('memcached')
const Sequelize = require('sequelize')
const Bluebird = require('bluebird')
const sequelizeCache = require('sequelize-transparent-cache')
const MemcachedAdaptor = require('sequelize-transparent-cache-memcached')
const requireDirectory = require('require-directory')

const { postgres, memcached } = require('../common/config').get()

module.exports = ({ logger }) => {
  const cache = new Memcached(
    `${memcached.host}:${memcached.port}`,
    { namespace: memcached.namespace }
  )

  const memcachedAdaptor = new MemcachedAdaptor({
    client: cache,
    namespace: 'model',
    lifetime: memcached.ttl,
  })

  const { withCache } = sequelizeCache(memcachedAdaptor)

  const sequelize = new Sequelize({
    dialect: 'postgres',
    // To enable logs use logger.info.bind(logger) as a value
    logging: false,
    replication: {
      write: postgres.write,
      read: [postgres.read],
    },
    define: {
      paranoid: true,
      indexes: [
        { fields: ['deletedAt'] },
      ],
    },
  })

  const modules = requireDirectory(module)

  const db = Object.values(modules).reduce((acc, module) => {
    const model = module(sequelize, Sequelize.DataTypes)

    acc[model.name] = withCache(model)

    return acc
  }, {})

  Object.values(db).forEach((model) => {
    if (model.associate) {
      model.associate(db)
    }
  })

  sequelize.cache = Bluebird.promisifyAll(cache)
  db.sequelize = sequelize
  db.Sequelize = Sequelize

  return db
}
