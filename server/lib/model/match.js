module.exports = (sequelize, DataTypes) => {
  const Match = sequelize.define('Match', {
    connectionId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'Connections',
        key: 'id',
      },
      onUpdate: 'cascade',
      onDelete: 'cascade',
    },
    recordType: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    recordId: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    issueId: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    isManual: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
  }, {
    indexes: [
      { fields: ['connectionId'] },
      { fields: ['issueId'] },
      { fields: ['createdAt'] },
      { fields: ['connectionId', 'issueId', 'recordType', 'recordId'], unique: true },
    ],
  })

  Match.associate = function associate (models) {
    this.belongsTo(models.Connection, { foreignKey: 'connectionId' })
  }

  return Match
}
