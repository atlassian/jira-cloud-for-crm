module.exports = (sequelize, DataTypes) => {
  const Account = sequelize.define('Account', {
    instanceId: {
      type: DataTypes.STRING,
      allowNull: false,
      references: {
        model: 'Instances',
        key: 'id',
      },
      onUpdate: 'cascade',
      onDelete: 'cascade',
    },
    jiraAccountId: {
      type: DataTypes.STRING,
      // TODO: change to false
      // We don't have AAID for all of users in production DB
      // so first need to figure out what to do with those records.
      // We can just fill them with empty value like '-'
      allowNull: true,
    },
    type: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    baseUrl: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    externalId: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    crmUserId: {
      type: DataTypes.STRING,
    },
    accessToken: {
      type: DataTypes.STRING(2000),
    },
    refreshToken: {
      type: DataTypes.STRING(2000),
    },
    tokenCreatedAt: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW,
    },
    isAuthorized: {
      type: DataTypes.VIRTUAL,
      get () {
        return !!this.get('accessToken') && !!this.get('refreshToken')
      },
    },
  }, {
    indexes: [
      { fields: ['instanceId'] },
      { fields: ['jiraAccountId'] },
      { fields: ['type'] },
      { fields: ['baseUrl'] },
      { fields: ['externalId'] },
      { fields: ['crmUserId'] },
      { fields: ['createdAt'] },
    ],
  })

  Account.associate = function associate (models) {
    this.belongsTo(models.Instance, { foreignKey: 'instanceId' })
    this.Connections = this.hasMany(models.Connection, {
      foreignKey: 'accountId',
      onDelete: 'cascade',
    })
  }

  Account.prototype.deactivate = function deactivate () {
    this.update({
      accessToken: null,
      refreshToken: null,
      tokenCreatedAt: null,
    })
  }

  return Account
}
