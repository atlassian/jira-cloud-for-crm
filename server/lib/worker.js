const express = require('express')
const { buildRoute } = require('./routes/util')
const statsd = require('./common/statsd')
const logger = require('./common/logger').child({ module: 'worker' })
const config = require('./common/config')
const { client } = require('./common/sqs/client')
const WorkersGroup = require('./common/sqs/WorkersGroup')
const initModel = require('./model')
const services = require('./services')

function initHandler ({ model, queueType }) {
  return async ({ Attributes, Body }) => {
    const duration = () => Date.now() - Attributes.SentTimestamp
    const parsedBody = JSON.parse(Body)
    const { context, data } = parsedBody
    const loggerWithContext = logger.child({ context })

    statsd.increment(queueType, { status: 'received' })

    try {
      const Service = services.workers[queueType]

      await new Service({
        model,
        context,
        logger: loggerWithContext,
      }).run(data || parsedBody)

      statsd.timing(queueType, duration(), { status: 'success' })
      statsd.increment(queueType, { status: 'sent' })
    } catch (error) {
      loggerWithContext.error({ err: error, sqsMessage: data })
      statsd.timing(queueType, duration(), { status: 'error' })
      statsd.increment(queueType, { status: 'not-sent' })
      statsd.increment('fatal_error', { source: error.source })
    }
  }
}

function initTracker ({ queueType }) {
  return function tracker (attrs) {
    Object.keys(attrs).forEach((attribute) => {
      statsd.gauge(`worker.${queueType}.${attribute}`, attrs[attribute])
    })

    statsd.gauge(`worker.${queueType}.workersCount`, this._workers.length)
  }
}

function startHealthCheckServer ({ model }) {
  /* istanbul ignore else */
  if (config.get('service.environment') !== 'production') {
    return
  }

  const route = buildRoute({ model, services })
  const app = express().get('/api/healthcheck', route('Middlewares.logger'), route('Healthcheck.get'))

  return new Promise((resolve, reject) => app.listen(
    8080,
    error => (error ? reject(error) : resolve())
  ))
}

function build () {
  const queueType = config.get('sqs.currentQueueType')
  const queue = config.get(`sqs.queues.${queueType}`)

  const model = initModel({ logger })
  const handler = initHandler({ queueType, model })
  const tracker = initTracker({ queueType })

  const worker = new WorkersGroup({ queue, logger, client, handler, tracker })

  return { model, worker }
}

async function start () {
  process.on('uncaughtException', (error) => {
    logger.fatal({ err: error }, 'Uncaught exception')
    process.exit(1)
  })

  process.on('unhandledRejection', (error) => {
    logger.error({ err: error }, 'Unhandled rejection')
  })

  const { model, worker } = build()

  await Promise.all([
    model.sequelize.authenticate(),
    worker.start(),
    startHealthCheckServer({ model }),
  ])

  return () => Promise.all([
    model.sequelize.close(),
    worker.stop(),
  ])
}

/* istanbul ignore if */
if (!module.parent) {
  start()
}

module.exports = {
  start,
  build,
}
