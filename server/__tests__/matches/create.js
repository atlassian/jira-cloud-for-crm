const { loadFixtures } = require('sequelize-fixtures')
const { Context, fixtures, utils } = require('../helpers')

const context = new Context()

beforeAll(() => context.begin())
afterAll(() => context.end())

beforeAll(() => loadFixtures(
  [...fixtures.instance, ...fixtures.accounts, ...fixtures.connections],
  context.model
))

describe('Matches.Create', () => {
  test('Create match', async () => {
    const { body } = await context.client
      .post('/api/matches')
      .send({
        projectId: '1000',
        recordType: 'Contact',
        recordId: 'CONTACTID0',
        issueId: '10001',
      })
      .set('Authorization', utils.auth.jira())
      .expect(201)

    expect(body).toHaveProperty('ok')
    expect(body).toHaveProperty('data')

    const match = body.data

    expect(match).toEqual({
      id: 1,
      recordType: 'Contact',
      recordId: 'CONTACTID0',
    })
  })

  test('Update match', async () => {
    const { body } = await context.client
      .post('/api/matches')
      .send({
        projectId: '1000',
        recordType: 'Account',
        recordId: 'ACCOUNTID0',
        issueId: '10002',
      })
      .set('Authorization', utils.auth.jira())
      .expect(201)

    expect(body).toHaveProperty('ok')
    expect(body).toHaveProperty('data')

    const match = body.data

    expect(match).toEqual({
      id: 2,
      recordType: 'Account',
      recordId: 'ACCOUNTID0',
    })
  })

  test('No connection was found', async () => {
    const { body } = await context.client
      .post('/api/matches')
      .send({
        projectId: '1111',
        recordType: 'Contact',
        recordId: 'CONTACTID0',
        issueId: '10000',
      })
      .set('Authorization', utils.auth.jira())
      .expect(400)

    expect(body.error).toEqual({
      message: 'Wrong project id',
      code: 'WRONG_ID',
      fields: ['projectId'],
    })
  })
})
