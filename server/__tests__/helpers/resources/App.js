const request = require('supertest')

class App {
  async start () {
    const appFactory = require('../../../lib/web') /* eslint global-require: 0 */
    const { model, app, server } = await appFactory.start(0)

    this.client = request(app)
    this.model = model
    this.app = app
    this.server = server

    await model.sequelize.sync()

    return this
  }

  async stop () {
    await new Promise((resolve, reject) => {
      this.server.close(error => (error ? reject(error) : resolve()))
    })

    await this.model.sequelize.close()
  }
}

module.exports = App
