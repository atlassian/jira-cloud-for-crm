const config = require('../../../lib/common/config')

class Worker {
  async start () {
    const worker = require('../../../lib/worker') /* eslint-disable-line global-require */

    await config.set('sqs.currentQueueType', 'GetIssues')
    const stopGetIssuesWorker = await worker.start()

    await config.set('sqs.currentQueueType', 'SyncIssue')
    const stopSyncIssueWorker = await worker.start()

    this._stop = [stopGetIssuesWorker, stopSyncIssueWorker]

    return this
  }

  stop () {
    return Promise.all(this._stop.map(stop => stop()))
  }
}

module.exports = Worker
