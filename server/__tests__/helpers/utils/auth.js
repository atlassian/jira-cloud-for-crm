const { format } = require('url')
const moment = require('moment')
const jwt = require('atlassian-jwt')
const crypto = require('crypto')
const mocks = require('../mocks')

const defaultInstance = {
  id: mocks.jira.hooks.instanceCreate.clientKey,
  sharedSecret: mocks.jira.hooks.instanceCreate.sharedSecret,
}

function jira ({ instance = defaultInstance } = {}) {
  const now = moment().utc()
  const hourLate = now.add(60, 'minutes').unix()

  return jwt.encode({
    sub: 'test:account-id',
    iss: instance.id,
    context: {},
    exp: hourLate,
    iat: now.unix(),
  }, instance.sharedSecret)
}

function hubspot ({ context, clientSecret = 'DUMMY', query = {} }) {
  const { port } = context.client.get('').app.address()
  const url = format({ pathname: `https://127.0.0.1:${port}/api/hubspot/issues`, query })
  const signatureString = `${clientSecret}GET${url}`

  return crypto.createHash('sha256').update(signatureString).digest('hex')
}

module.exports = {
  jira,
  hubspot,
}
