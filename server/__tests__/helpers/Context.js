const uuid = require('uuid/v1')
const nock = require('nock')
const resources = require('./resources')
const config = require('../../lib/common/config')

class Context {
  constructor (options = { app: true, worker: false, cache: false }) {
    // Disable network requests caching for the most of tests
    if (!options.cache) {
      config.set('service.isExternalApiCallsCached', false)
    }

    jest.setTimeout(30000)

    this.options = options
    this.contextId = uuid()
    this.resources = []
    this.model = null
    this.client = null
    this.app = null
  }

  async begin () {
    const deps = ['Memcached', 'Postgres']

    if (this.options.worker) {
      deps.push('SQS')
    }

    const promises = deps.map((key) => {
      const resource = new resources[key](this.contextId)

      return resource.init()
    })

    this.resources = await Promise.all(promises)

    const startApp = (new resources.App()).start()
    const startWorker = this.options.worker
      ? (new resources.Worker()).start()
      : undefined

    const [app, worker] = await Promise.all([startApp, startWorker])

    this.model = app.model
    this.client = app.client

    this.app = app
    this.worker = worker

    return this
  }

  async end () {
    const pendingMocks = nock.pendingMocks()

    if (pendingMocks.length) {
      // eslint-disable-next-line no-console
      console.error('pending mocks: %j', pendingMocks)
    }

    nock.cleanAll()
    nock.restore()

    await Promise.all([
      this.app && this.app.stop(),
      this.worker && this.worker.stop(),
    ])
    await Promise.all(this.resources.map(resource => resource.clean()))
  }
}

module.exports = Context
