const nock = require('nock')
const moment = require('moment')
const requireDirectory = require('require-directory')

const { hooks, responses } = requireDirectory(module)

const { baseUrl: instanceBaseUrl } = hooks.instanceCreate

function setProjectProperty (projectId = '1000') {
  return nock(instanceBaseUrl)
    .put(`/rest/api/2/project/${projectId}/properties/jira-crm-integration`)
    .reply(200)
}

function deleteProjectProperty (projectId = '1000') {
  return nock(instanceBaseUrl)
    .delete(`/rest/api/2/project/${projectId}/properties/jira-crm-integration`)
    .reply(200)
}

function getBearerToken () {
  return nock('https://auth.prod.public.atl-paas.net')
    .post('/oauth2/token')
    .query(true)
    .reply(200, {
      access_token: 'TEST',
      expires_in: moment().utc().add(1, 'hour').unix(),
    })
}

function getProjectPermissions (response = responses.projectPermissions) {
  return nock(instanceBaseUrl)
    .get('/rest/api/2/mypermissions')
    .query(true)
    .reply(200, response)
}

function installAddon () {
  return nock(instanceBaseUrl)
    .post('/rest/atlassian-connect/1/systemaddons/install')
    .reply(200, {
      addonKey: 'jira-crm-integration',
    })
}

function getIssue () {
  return nock(instanceBaseUrl)
    .get(`/rest/api/2/issue/${responses.issue.id}`)
    .reply(200, responses.issue)
}

function searchIssues () {
  return nock(instanceBaseUrl)
    .post('/rest/api/3/search')
    .reply(200, responses.searchIssues)
}

function getUserEmail (email) {
  return nock(instanceBaseUrl)
    .get('/rest/api/3/user/email')
    .query(true)
    .reply(200, email
      ? { ...responses.userEmail, email }
      : responses.userEmail)
}

module.exports = {
  setProjectProperty,
  deleteProjectProperty,
  getBearerToken,
  getProjectPermissions,
  installAddon,
  getIssue,
  searchIssues,
  getUserEmail,
}
