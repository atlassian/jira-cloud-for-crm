const { loadFixtures } = require('sequelize-fixtures')
const { Context, fixtures, mocks, utils } = require('../helpers')

const context = new Context()

beforeAll(() => context.begin())
afterAll(() => context.end())

beforeAll(() => loadFixtures(
  [...fixtures.instance, ...fixtures.accounts, ...fixtures.connections],
  context.model
))

describe('Issues.List', () => {
  test('Get Issues list for Hubspot contact with no match', async () => {
    const query = {
      portalId: '00hubspot00',
      associatedObjectId: '101',
      associatedObjectType: 'CONTACT',
    }

    const { body } = await context.client
      .get('/api/hubspot/issues')
      .query(query)
      .set('x-hubspot-signature', utils.auth.hubspot({ context, query }))
      .expect(200)

    expect(body).toHaveProperty('results')
    expect(body.results).toHaveLength(1)

    expect(body.results).toEqual([
      {
        objectId: null,
        title: 'There are no Jira issues associated with this contact.',
      }])
  })

  test('Get Issues list for Hubspot contact with match', async () => {
    mocks.jira.listeners.searchIssues()

    await loadFixtures(
      [...fixtures.matches],
      context.model
    )

    const query = {
      portalId: '00hubspot00',
      associatedObjectId: '101',
      associatedObjectType: 'CONTACT',
    }

    const { body } = await context.client
      .get('/api/hubspot/issues')
      .query(query)
      .set('x-hubspot-signature', utils.auth.hubspot({ context, query }))
      .expect(200)

    expect(body).toHaveProperty('results')
    expect(body.results).toHaveLength(2)

    expect(body.results).toEqual([
      {
        objectId: '10001',
        link: 'https://main-jira-in-the-w0rld.jira.com/browse/DEV-2',
        title: 'DEV-2',
        properties: [
          { dataType: 'STRING', label: 'Summary', value: 'Debugging' },
          { dataType: 'STATUS', label: 'Status', optionType: 'SUCCESS', value: 'Done' },
          { dataType: 'STRING', label: 'Assignee', value: 'Unassigned' },
          { dataType: 'STRING', label: 'Reporter', value: 'Test User' },
          { dataType: 'STRING', label: 'Priority', value: 'Low' },
          { dataType: 'STRING', label: 'Type', value: 'Story' },
        ],
      },
      {
        objectId: '10000',
        link: 'https://main-jira-in-the-w0rld.jira.com/browse/DEV-1',
        title: 'DEV-1',
        properties: [
          { dataType: 'STRING', label: 'Summary', value: 'Test Issue' },
          { dataType: 'STATUS', label: 'Status', optionType: 'DEFAULT', value: 'To Do' },
          { dataType: 'STRING', label: 'Assignee', value: 'Unassigned' },
          { dataType: 'STRING', label: 'Reporter', value: 'Test User' },
          { dataType: 'STRING', label: 'Priority', value: 'Medium' },
          { dataType: 'STRING', label: 'Type', value: 'Story' },
        ],
      }])
  })
})
