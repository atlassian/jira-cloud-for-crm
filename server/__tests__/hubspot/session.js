const crypto = require('crypto')
const { loadFixtures } = require('sequelize-fixtures')
const { Context, fixtures, utils } = require('../helpers')

const context = new Context()

beforeAll(() => context.begin())
afterAll(() => context.end())

beforeAll(() => loadFixtures(
  [...fixtures.instance, ...fixtures.accounts, ...fixtures.connections],
  context.model
))

describe('Hubspot.Session', () => {
  test('Request without x-hubspot-signature header', async () => {
    const { body } = await context.client
      .get('/api/hubspot/issues')
      .expect(401)

    expect(body).toEqual({
      error: { code: 'UNAUTHORIZED' },
    })
  })

  test('Request with invalid x-hubspot-signature header', async () => {
    const { body } = await context.client
      .get('/api/hubspot/issues')
      .set('x-hubspot-signature', crypto.createHash('sha256').update('INVALID SIGNATURE').digest('hex'))
      .expect(401)

    expect(body).toEqual({
      error: { code: 'UNAUTHORIZED' },
    })
  })

  test('Request with correct x-hubspot-signature header', async () => {
    const { body } = await context.client
      .get('/api/hubspot/issues')
      .query({ portalId: 'test' })
      .set('x-hubspot-signature', utils.auth.hubspot({ context, query: { portalId: 'test' } }))
      .expect(200)

    expect(body).toEqual({
      results: [{
        objectId: null,
        link: 'https://marketplace.atlassian.com/plugins/jira-crm-integration/cloud/overview',
        title: 'Connect Jira project to HubSpot to see issues here',
      }],
    })
  })
})
