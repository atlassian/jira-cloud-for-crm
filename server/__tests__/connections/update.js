const { loadFixtures } = require('sequelize-fixtures')
const { Context, fixtures, mocks, utils } = require('../helpers')

const context = new Context()

beforeAll(() => context.begin())
afterAll(() => context.end())

beforeAll(() => loadFixtures(
  [...fixtures.instance, ...fixtures.accounts, ...fixtures.connections],
  context.model
))

describe('Connections.Update', () => {
  test('Update connection', async () => {
    const { id, accountId, projectId } = fixtures.connections[0].data

    const newOptions = {
      schema: [
        {
          id: 'Test Id',
          label: 'Test Label',
          fields: [{ id: 'field1', label: 'Field1', type: 'string' }],
          objects: [
            {
              id: 'Test Object Id',
              label: 'Test Object',
              fields: [{ id: 'obj field1', label: 'obj Field1', type: 'string' }],
            },
          ],
        },
      ],
    }

    const { body } = await context.client
      .put(`/api/connections/${id}`)
      .send({ options: newOptions })
      .set('Authorization', utils.auth.jira())
      .expect(201)

    expect(body).toHaveProperty('ok')
    expect(body).toHaveProperty('data')

    expect(body.data.id).toBe(id)
    expect(body.data.accountId).toBe(accountId)
    expect(body.data.projectId).toBe(projectId)
    expect(body.data.options).toEqual(newOptions)
  })

  test('Update connection with invalid schema', async () => {
    const { id } = fixtures.connections[0].data

    const newOptions = {
      schema: [{ foo: 'bar' }],
    }

    const { body } = await context.client
      .put(`/api/connections/${id}`)
      .send({ options: newOptions })
      .set('Authorization', utils.auth.jira())
      .expect(400)

    expect(body).toHaveProperty('error')
    expect(body.error.code).toBe('FORMAT_ERROR')
  })

  test.skip('Update connection without admin permission', async () => {
    mocks.jira.listeners.getBearerToken()
    mocks.jira.listeners.getProjectPermissions({
      permissions: {
        ADMINISTER_PROJECTS: { havePermission: false },
        PROJECT_ADMIN: { havePermission: false },
      },
    })

    const { id } = fixtures.connections[0].data
    const newOptions = { schema: [] }

    const jiraToken = utils.auth.jira({
      jiraAccountId: 'not-admin',
    })

    const { body } = await context.client
      .put(`/api/connections/${id}`)
      .send({ options: newOptions })
      .set('Authorization', jiraToken)
      .expect(400)

    expect(body).toHaveProperty('error')
    expect(body.error.code).toBe('PERMISSION_DENIED')
  })

  test('No connection was found', async () => {
    const newOptions = {
      schema: [
        {
          id: 'Test Id',
          label: 'Test Label',
          fields: [{ id: 'field1', label: 'Field1', type: 'string' }],
          objects: [
            {
              id: 'Test Object Id',
              label: 'Test Object',
              fields: [{ id: 'obj field1', label: 'obj Field1', type: 'string' }],
            },
          ],
        },
      ],
    }

    const { body } = await context.client
      .put('/api/connections/111')
      .send({ options: newOptions })
      .set('Authorization', utils.auth.jira())
      .expect(400)

    expect(body.error).toEqual({
      message: 'Wrong connection id',
      code: 'WRONG_ID',
      fields: ['id'],
    })
  })
})
