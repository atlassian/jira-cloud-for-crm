const { loadFixtures } = require('sequelize-fixtures')
const nock = require('nock')
const { Context, fixtures, mocks, utils } = require('../helpers')
const salesforceDefaultSchema = require('../../lib/common/crm/Salesforce/defaultSchema.json')
const dynamicsDefaultSchema = require('../../lib/common/crm/Dynamics/defaultSchema.json')
const hubspotDefaultSchema = require('../../lib/common/crm/Hubspot/defaultSchema.json')

const context = new Context()

beforeAll(() => context.begin())
afterAll(() => context.end())

beforeAll(() => loadFixtures(
  [...fixtures.instance, ...fixtures.accounts, ...fixtures.connections, ...fixtures.matches],
  context.model
))

describe('Customers.Show', () => {
  test('Get customer without match', async () => {
    const { body } = await context.client
      .get('/api/customers/projects/1000/issues/1337')
      .set('Authorization', utils.auth.jira())
      .expect(200)

    expect(body).toHaveProperty('ok')
    expect(body).toHaveProperty('data')
    expect(body.data).toEqual({
      accountType: 'salesforce',
      customers: [],
    })
  })

  test('Get Salesforce customer', async () => {
    mocks.salesforce.listeners.getContactById()

    await context.model.Connection.update(
      { options: { schema: salesforceDefaultSchema } },
      { where: { id: 1 } }
    )

    const { body } = await context.client
      .get('/api/customers/projects/1000/issues/10000')
      .set('Authorization', utils.auth.jira())
      .expect(200)

    expect(body).toHaveProperty('ok')
    expect(body).toHaveProperty('data')

    expect(body.data.accountType).toBe('salesforce')
    expect(body.data).toHaveProperty('customers')
    expect(body.data.customers).toHaveLength(1)

    const [customer] = body.data.customers

    expect(customer.values).toHaveLength(2)
    expect(customer).toHaveProperty('matchId')
    expect(customer).toHaveProperty('isManual')

    const [contact, account] = customer.values

    expect(contact.values).toEqual(['test@mail.com'])
    expect(contact.info).toEqual({
      recordId: 'CONTACTID0',
      title: 'John Doe',
      url: 'https://test1.salesforce.com/CONTACTID0',
    })

    expect(account.values).toEqual(['Test Type'])
    expect(account.info).toEqual({
      recordId: 'ACCOUNTID0',
      title: 'Test Account',
      url: 'https://test1.salesforce.com/ACCOUNTID0',
    })
  })

  test('Failed to get Salesforce account information', async () => {
    const { baseUrl } = fixtures.accounts[0].data

    nock(baseUrl)
      .get('/services/data/v40.0/query')
      .query(true)
      .reply(400, () => ({
        error: 'invalid_grant',
        error_description: 'inactive user',
      }))

    const { body } = await context.client
      .get('/api/customers/projects/1000/issues/10000')
      .set('Authorization', utils.auth.jira())
      .expect(400)

    expect(body.error.code).toBe('INVALID_CLIENT')
    expect(body.error.message).toBe('CRM account needs authorization')
  })

  test('Salesforce redundant field results in bad request', async () => {
    const { baseUrl } = fixtures.accounts[0].data

    nock(baseUrl)
      .get('/services/data/v40.0/query')
      .query(true)
      .reply(400, () => ([{
        errorCode: 'INVALID_FIELD',
        message: `Account.Id, Account.Name
        ^
        ERROR at Row:1:Column:405
        No such column 'Name' on entity 'Account'. If you are attempting to use a custom field, be sure to append the '__c' after the custom field name. Please reference your WSDL or the describe call for the appropriate names.`,
      }]))

    const { contactWithAccount } = mocks.salesforce.responses
    const contactWithoutAccountName = {
      ...contactWithAccount,
      records: contactWithAccount.records.map((r) => {
        const { Name, ...updatedAccount } = r.Account

        return {
          ...r,
          Account: updatedAccount,
        }
      }),
    }

    nock(baseUrl)
      .get('/services/data/v40.0/query')
      .query({
        q: 'select Id, Name, Account.Id, Account.Name, Contact.Email, Account.Type from Contact where id = \'CONTACTID0\'',
      })
      .reply(200, contactWithoutAccountName)

    const { body } = await context.client
      .get('/api/customers/projects/1000/issues/10000')
      .set('Authorization', utils.auth.jira())
      .expect(200)

    expect(body).toHaveProperty('ok')
    expect(body).toHaveProperty('data')

    expect(body.data.accountType).toBe('salesforce')
    expect(body.data).toHaveProperty('customers')
    expect(body.data.customers).toHaveLength(1)

    const customersAccount = body.data.customers[0].values.find(v => v.id === 'Account')
    const removedField = customersAccount.fields.find(f => f.id === 'Name')

    expect(removedField).toBe(undefined)
    expect(removedField).not.toBe(contactWithAccount.records[0].Account.Name)
  })

  test('Get Salesforce Cases by Contact', async () => {
    mocks.salesforce.listeners.getCasesByContact()

    const { body } = await context.client
      .get('/api/customers/projects/1000/issues/10000')
      .query({
        recordId: 'CONTACTID0',
        recordType: 'Contact',
        childRecordType: 'Case',
      })
      .set('Authorization', utils.auth.jira())
      .expect(200)

    expect(body).toHaveProperty('ok')
    expect(body).toHaveProperty('data')

    const [cases] = body.data

    expect(cases.values).toEqual([
      ['0123456', 'Test subject', 'Test status'],
    ])
    expect(cases.url).toEqual([
      'https://test1.salesforce.com/CASEID0',
    ])
  })

  test('Get Salesforce Opportunities by Contact', async () => {
    mocks.salesforce.listeners.getOpportunitiesByContact()

    const { body } = await context.client
      .get('/api/customers/projects/1000/issues/10000')
      .query({
        recordId: 'CONTACTID0',
        recordType: 'Contact',
        childRecordType: 'Opportunity',
      })
      .set('Authorization', utils.auth.jira())
      .expect(200)

    expect(body).toHaveProperty('ok')
    expect(body).toHaveProperty('data')

    const [opportunities] = body.data

    expect(opportunities.values).toEqual([
      ['Dummy Opp', 'Stage 1', '2017-08-31'],
      ['Grand Hotels Guest Portable Generators', 'Stage 2', '2017-05-29'],
      ['Awesome opportunity', 'Stage 3', '2017-07-31'],
    ])
    expect(opportunities.url).toEqual([
      'https://test1.salesforce.com/OPPORTUNITYID1',
      'https://test1.salesforce.com/OPPORTUNITYID2',
      'https://test1.salesforce.com/OPPORTUNITYID3',
    ])
  })

  test('Get Salesforce Entitlements by Account', async () => {
    mocks.salesforce.listeners.getEntitlementsByAccount()

    const { body } = await context.client
      .get('/api/customers/projects/1000/issues/10000')
      .query({
        recordId: 'ACCOUNTID0',
        recordType: 'Account',
        childRecordType: 'Entitlement',
      })
      .set('Authorization', utils.auth.jira())
      .expect(200)

    expect(body).toHaveProperty('ok')
    expect(body).toHaveProperty('data')

    const [entitlements] = body.data

    expect(entitlements.values).toEqual([
      [
        'Lisence MIT',
        'Expired',
        'Phone Support',
        '2017-09-12',
        '2017-09-30',
        { name: 'Laptop', url: 'https://test1.salesforce.com/ASSETID0' },
      ],
    ])
    expect(entitlements.url).toEqual([
      'https://test1.salesforce.com/ENTITLEMENTID0',
    ])
  })

  test('Salesforce Entitlement management is disabled', async () => {
    mocks.salesforce.listeners.entitlementsIsDisabled()

    const { body } = await context.client
      .get('/api/customers/projects/1000/issues/10000')
      .query({
        recordId: 'ACCOUNTID0',
        recordType: 'Account',
        childRecordType: 'Entitlement',
      })
      .set('Authorization', utils.auth.jira())
      .expect(400)

    expect(body).toHaveProperty('error')
    expect(body.error.code).toEqual('OBJECT_DISABLED')
  })

  test('Get Dynamics customer', async () => {
    mocks.dynamics.listeners.getContactById()
    mocks.dynamics.listeners.getAccountById()

    await context.model.Connection.update(
      { options: { schema: dynamicsDefaultSchema } },
      { where: { id: 2 } }
    )

    const { body } = await context.client
      .get('/api/customers/projects/1001/issues/10000')
      .set('Authorization', utils.auth.jira())
      .expect(200)

    expect(body).toHaveProperty('ok')
    expect(body).toHaveProperty('data')

    expect(body.data.accountType).toBe('dynamics')
    expect(body.data).toHaveProperty('customers')

    const [customer] = body.data.customers

    expect(customer.values).toHaveLength(2)
    expect(customer).toHaveProperty('matchId')
    expect(customer).toHaveProperty('isManual')

    const [contact, account] = customer.values

    expect(contact.values).toEqual(['test@mail.com'])
    expect(contact.info).toEqual({
      recordId: 'CONTACTID0',
      title: 'John Doe',
      url: 'https://test.crm.dynamics.com/main.aspx?pagetype=entityrecord&etn=contact&id=CONTACTID0',
    })

    expect(account.values).toEqual(['account@mail.com'])
    expect(account.info).toEqual({
      recordId: 'ACCOUNTID0',
      title: 'Test Account',
      url: 'https://test.crm.dynamics.com/main.aspx?pagetype=entityrecord&etn=account&id=ACCOUNTID0',
    })
  })

  test('Dynamics redundant field results in bad request', async () => {
    mocks.dynamics.listeners.getAccountById()
    const { baseUrl } = fixtures.accounts[1].data
    const invalidFieldId = 'emailaddress1'

    nock(baseUrl)
      .get('/api/data/v8.2/contacts')
      .query({
        $select: `fullname,_parentcustomerid_value,${invalidFieldId}`,
        $filter: 'contactid eq CONTACTID0',
      })
      .reply(400, () => ({
        error: {
          code: '0x0',
          innererror: {
            type: 'Microsoft.OData.ODataException',
          },
          message: `Could not find a property named '${invalidFieldId}' on type 'Microsoft.Dynamics.CRM.contact'.`,
        },
      }))

    const { contact } = mocks.dynamics.responses

    const contactWithoutEmail = {
      ...contact,
      value: contact.value.map((value) => {
        const { emailaddress1, ...updatedContact } = value

        return updatedContact
      }),
    }

    nock(baseUrl)
      .get('/api/data/v8.2/contacts')
      .query({
        $select: 'fullname,_parentcustomerid_value',
        $filter: 'contactid eq CONTACTID0',
      })
      .reply(200, contactWithoutEmail)

    const { body } = await context.client
      .get('/api/customers/projects/1001/issues/10000')
      .set('Authorization', utils.auth.jira())
      .expect(200)

    expect(body).toHaveProperty('ok')
    expect(body).toHaveProperty('data')

    expect(body.data.accountType).toBe('dynamics')
    expect(body.data).toHaveProperty('customers')
    expect(body.data.customers).toHaveLength(1)

    const customersAccount = body.data.customers[0].values.find(v => v.id === 'Contact')
    const removedField = customersAccount.fields.find(f => f.id === invalidFieldId)

    expect(removedField).toBe(undefined)
    expect(removedField).not.toBe(contact.value[0][invalidFieldId])
  })

  test('Get Dynamics Cases by Contact', async () => {
    mocks.dynamics.listeners.getCasesByContact()

    const { body } = await context.client
      .get('/api/customers/projects/1001/issues/10000')
      .query({
        recordId: 'CONTACTID0',
        recordType: 'Contact',
        childRecordType: 'Incident',
      })
      .set('Authorization', utils.auth.jira())
      .expect(200)

    expect(body).toHaveProperty('ok')
    expect(body).toHaveProperty('data')

    const [cases] = body.data

    expect(cases.values).toEqual([
      [
        'Test case',
        '0123456',
        '2017-09-27T11:28:07Z',
        'Active',
        null,
        'Normal',
      ],
    ])
    expect(cases.url).toEqual([
      'https://test.crm.dynamics.com/main.aspx?pagetype=entityrecord&etn=incident&id=INCIDENID0',
    ])
  })

  test('Get Dynamics Opportunities by Contact (empty)', async () => {
    mocks.dynamics.listeners.getOpportunitiesByContact()

    const { body } = await context.client
      .get('/api/customers/projects/1001/issues/10000')
      .query({
        recordId: 'CONTACTID0',
        recordType: 'Contact',
        childRecordType: 'Opportunity',
      })
      .set('Authorization', utils.auth.jira())
      .expect(200)

    expect(body).toHaveProperty('ok')
    expect(body).toHaveProperty('data')

    const [opportunities] = body.data

    expect(opportunities.values).toEqual([])
    expect(opportunities.url).toEqual([])
  })

  test('Get Dynamics Contacts by Account', async () => {
    mocks.dynamics.listeners.getContactsByAccount()

    const { body } = await context.client
      .get('/api/customers/projects/1001/issues/10000')
      .query({
        recordId: 'ACCOUNTID0',
        recordType: 'Account',
        childRecordType: 'Contact',
      })
      .set('Authorization', utils.auth.jira())
      .expect(200)

    expect(body).toHaveProperty('ok')
    expect(body).toHaveProperty('data')

    const [contacts] = body.data

    expect(contacts.values).toEqual([
      ['John Doe', 'test1@mail.com'],
      ['Travis Brown', 'test2@mail.com'],
      ['Jessie Jay', 'test3@mail.com'],
    ])
    expect(contacts.url).toEqual([
      'https://test.crm.dynamics.com/main.aspx?pagetype=entityrecord&etn=contact&id=CONTACTID1',
      'https://test.crm.dynamics.com/main.aspx?pagetype=entityrecord&etn=contact&id=CONTACTID2',
      'https://test.crm.dynamics.com/main.aspx?pagetype=entityrecord&etn=contact&id=CONTACTID3',
    ])
  })

  test('Get HubSpot customer', async () => {
    mocks.hubspot.listeners.getContactById()
    mocks.hubspot.listeners.getContactProperties()

    await context.model.Connection.update(
      { options: { schema: hubspotDefaultSchema } },
      { where: { id: 3 } }
    )

    const { body } = await context.client
      .get('/api/customers/projects/1002/issues/10000')
      .set('Authorization', utils.auth.jira())
      .expect(200)

    expect(body).toHaveProperty('ok')
    expect(body).toHaveProperty('data')

    expect(body.data.accountType).toBe('hubspot')
    expect(body.data).toHaveProperty('customers')

    const [customer] = body.data.customers

    expect(customer.values).toHaveLength(2)
    expect(customer).toHaveProperty('matchId')
    expect(customer).toHaveProperty('isManual')

    const [contact, account] = customer.values

    expect(contact.values).toEqual(['test@mail.com'])
    expect(contact.info).toEqual({
      recordId: '101',
      title: 'John Doe',
      url: 'https://app.hubspot.com/contacts/00hubspot00/contact/101',
    })

    expect(account.values).toEqual(['Test Type'])
    expect(account.info).toEqual({
      recordId: '100000000',
      title: 'Test Account',
      url: 'https://app.hubspot.com/contacts/00hubspot00/company/100000000',
    })
  })

  test('Get HubSpot customer failed', async () => {
    mocks.hubspot.listeners.getContactById404()

    const { body } = await context.client
      .get('/api/customers/projects/1002/issues/10000')
      .set('Authorization', utils.auth.jira())
      .expect(200)

    const { accountType, customers } = body.data

    expect(body).toHaveProperty('ok')
    expect(body).toHaveProperty('data')

    expect(accountType).toBe('hubspot')
    expect(customers).toHaveLength(0)
  })

  test('Get HubSpot Deals by Contact', async () => {
    mocks.hubspot.listeners.getDealsByContact()
    mocks.hubspot.listeners.getPipelineById()

    const { body } = await context.client
      .get('/api/customers/projects/1002/issues/10000')
      .query({
        recordId: '101',
        recordType: 'Contact',
        childRecordType: 'Deal',
      })
      .set('Authorization', utils.auth.jira())
      .expect(200)

    expect(body).toHaveProperty('ok')
    expect(body).toHaveProperty('data')

    const [deals] = body.data

    expect(deals.values).toEqual([
      ['Test deal 01', 'Contract Sent', 1506718800000],
      ['Test deal 02', 'Presentation Scheduled', 1509400800000],
    ])
    expect(deals.url).toEqual([
      'https://app.hubspot.com/contacts/00hubspot00/deal/1001',
      'https://app.hubspot.com/contacts/00hubspot00/deal/1002',
    ])
  })

  test('Get HubSpot Contacts by Company', async () => {
    mocks.hubspot.listeners.getContactsByCompany()

    const { body } = await context.client
      .get('/api/customers/projects/1002/issues/10000')
      .query({
        recordId: '100000000',
        recordType: 'Company',
        childRecordType: 'Contact',
      })
      .set('Authorization', utils.auth.jira())
      .expect(200)

    expect(body).toHaveProperty('ok')
    expect(body).toHaveProperty('data')

    const [contacts] = body.data

    expect(contacts.values).toEqual([
      ['John Doe', 'test1@mail.com'],
      ['Travis Brown', 'test2@mail.com'],
      ['Customer', 'test3@mail.com'],
    ])
    expect(contacts.url).toEqual([
      'https://app.hubspot.com/contacts/00hubspot00/contact/101',
      'https://app.hubspot.com/contacts/00hubspot00/contact/102',
      'https://app.hubspot.com/contacts/00hubspot00/contact/103',
    ])
  })

  test('No connection', async () => {
    await context.model.Connection.destroy({
      where: { id: 1 },
      force: true,
    })

    const response = await context.client
      .get('/api/customers/projects/1000/issues/10000')
      .set('Authorization', utils.auth.jira())
      .expect(400)

    const { error } = response.body

    expect(error).toEqual({
      message: 'No connection for current project',
      code: 'NO_CONNECTION',
    })
  })
})
